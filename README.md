Introduction
============

TRooFit is a collection of pdf classes for RooFit, that are designed to behave like objects from ROOT. For example, TRooFit introduces:

    - TRooH1D : A pdf (inherits from RooAbsPdf) that you can fill and draw like a TH1D. The value of this object is a DENSITY.
    - TRooHF1D : A function (inherits from RooAbsReal). The value of this object is a VALUE.
    - TRooHStack : A sum of TRooH1D (or any other RooAbsReal) ... think of it as a stack of histograms
    - TRooHPdfStack : Like a TRooHStack but only accepts (extended) pdfs, and ensures each is normalized individually in the summation, i.e. pdfValue = Sum(i) coef_i*pdfValue_i, where coef_i is the fraction of total expected events of that pdf. The coef_i are automatically recomputed 

TRooFit also includes high-level classes for helping you build models and perform statistical analysis. Most important of this is the ```TRooWorkspace```, which inherits from ```RooWorkspace``` but comes with extra features and functions.

Setup (Standalone on top of ROOT)
=====

To setup TRooFit on top of a ROOT release, you can compile it as a standalone CMake project like this:

```
cmake path/to/TRooFit
make
```

Depending on your system, you then may be able to install the project with:

```
make install
```

This should work e.g. on a laptop where you have permissions to write to /usr/local. Otherwise, to 
use TRooFit from the interactive prompt, you must start ROOT from the build directory.

Setup (ATLAS Analysis releases)
=====

Setup any release that has ROOT, e.g. AthAnalysis, and then use acm to clone this package and compile it (this will compile the master version of TRooFit)

```
mkdir build source
cd build
acmSetup AthAnalysis,21.2.150
acm clone_project TRooFit will/TRooFit
acm find_packages
acm compile
````



Documentation
========
Example notebooks are available to help you get started ....

1a. [Building a simple s+b model](https://cernbox.cern.ch/index.php/s/WttQzxbWt9nYkOs)<br>
1b. [Model building with TRooWorkspaces](https://cernbox.cern.ch/index.php/s/dxJoaZC0W131OgZ)<br>
1e. [Adding external constraints to an existing model](https://cernbox.cern.ch/index.php/s/p3EjPBwBvC7Wdj6)<br>
1f. [FAST Model building and Limit setting](https://cernbox.cern.ch/index.php/s/OorOnfCGSWKu7py) - <b>NEW October 2019</b><br>
2a. [Model inspection general demo](https://cernbox.cern.ch/index.php/s/tzyUPsHqBSgKK1A)<br>
2b. [Obtaining yields and uncertainties](https://cernbox.cern.ch/index.php/s/I7dwstfWVMD2CIk)<br>
2c. [Parameter estimation and systematic breakdown](https://cernbox.cern.ch/index.php/s/Mi2MtN8TvAdHocf)<br>

3a. [Basic limit setting (with toys)](https://cernbox.cern.ch/index.php/s/bxGGvq0yRZwa8yd)<br>
3b. [Limit setting with asymptotic formulae](https://cernbox.cern.ch/index.php/s/hYP5y6oT9futwsu)<br>
3c. [Expected and observed discovery significance with asymptotics](https://cernbox.cern.ch/index.php/s/ku7ls0xcxizg6rp)<br>

Binder
=======
[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gh/will-cern/TRooFit-binder/master)

