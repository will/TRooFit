/*****************************************************************************
 * Project: TRooFit   - an extension for RooFit                              *
 *                                                                           *
 * Modified version of a 
 * RooWorkspace ... Helps with model building                          * 
 *****************************************************************************/

#ifndef TROOWORKSPACE
#define TROOWORKSPACE

//next lines to hack RooFitResult so that statusHistory can be presered when fitTo
#define protected public
#include "RooFitResult.h"
#undef protected

#define private protected
// added workspace here so can access _namedSets to clear caches too
#include "RooWorkspace.h"
#undef private


#include "TRooFit/TRooHStack.h"
#include "TRooFit/TRooH1.h"
#include "TRooFit/TRooHF1.h"

#include "TTree.h"
#include "TLegend.h"

#include "RooSimultaneous.h"
 
class TRooWorkspace : public RooWorkspace {
public:
  //using RooWorkspace::RooWorkspace; 
  
  
  TRooWorkspace() : RooWorkspace() { }
  TRooWorkspace(const char* name, const char* title = 0) : RooWorkspace(name,title) { }
  TRooWorkspace(const RooWorkspace& other);
  ~TRooWorkspace() { fNll.removeAll(); }

  /**
   * This is the new 2020 interface for building up a model by adding ROOT objects to the workspace
   * The behaviour of the function depends on the type of object added:
   *
   * Histogram (TH1):
   *   Creates or adds to a TRooH1 (a sample or a factor) or RooDataset (for data)
   *   use the following naming convention for histograms:
   *         <sample_name>;<channel_name>;<variation_name>=<value>
   *
   *   If sample_name is missing, the histogram is interpreted as data for channel_name.
   *   If channel_name is missing, the histogram is interpreted as a factor called sample_name.
   *   variation_name and value are optional, and specify if the histogram is interpreted as a
   *   systematic variation (meaningless in the case of data).
   *
   * Axis (TAxis):
   *   Creates a variable (argument, parameter, or observable), or adds a new range (for
   *   parameters) or binning (for observables) to an existing variable. Use naming convention
   *   for axis:
   *        <variable_name>;<binning/range_name>
   *
   *
   * @param obj
   * @return
   */
  bool Add(TObject& obj);

  /**
   * Scale samples by any function
   * @param obj1 - samples to scale, in notation: <sample_name>;<channel_name>
   * @param obj2
   * @return
   */
  //bool Scale(const char* samples, const char* func);


private:
    bool _add(TH1& h1);
    bool _add(TAxis& ax);

public:

  //-------

  bool definePoi(const char* poi) { return defineSet("poi",poi); }

    /**
    * TRooFit splits RooFit variables (RooRealVar) into three categories: Arguments, Parameters,
    * and Observables. All can be represented using a TAxis object. The differences between them
    * are as follows:
    *   - Arguments are constants, not allowed to float in fits. They have at least 1 of their
    *   min/max values set to infinity (usually both)
    *   - Parameters float in fits, and must therefore have a well defined range (min and max).
    *   But they do not have a binning (0 bins)
    *   - Observables are values that will appear in datasets, and are used as the axes for
    *   channels. They are TAxis with non-infinite ranges and at least 1 bin
    * @param ax - the TAxis providing the configuration for the desired variable
    * @return - the variable
    */
    RooRealVar* addVariable(const TAxis& ax);

    // helper functions for calling addVariable above
    RooRealVar* addArgument(const char* name, const char* title, double val);
    RooRealVar* addObservable(const char* name, const char* title, double min, double max);
    RooRealVar* addObservable(const char* name, const char* title, int nBins, double min, double max);
    RooRealVar* addObservable(const char* name, const char* title, int nBins, const double* bins);

    /**
    * Adds a variable to the workspace that is allowed to float in fits
    * @param name - parameter name
    * @param title - parameter title
    * @param val - parameter initial value
    * @param min - minimum parameter value
    * @param max - maximum parameter value
    * @param constraintType - if specified, will add constraint term to the model for this
    *   parameter. Choose from "NORMAL" (=GAUSSIAN(0,1)) and "GAUSSIAN(x,y)" where x is the global
    *   observable and y is the standard deviation
    * @return the variable on success
    */
    RooRealVar* addParameter(const char* name, const char* title, double val, double min, double max, const char* constraintType=0);
    RooRealVar* addParameter(const char* name, const char* title, double min, double max, const char* constraintType=0);



  TRooHStack* addChannel(const char* name, const char* title, const char* observable, int nBins, double min, double max);
  TRooHStack* addChannel(const char* name, const char* title, const char* observable, int nBins, const double* bins);
  TRooHStack* addChannel(const char* name, const char* title, const char* observable);
  TRooAbsHStack* addChannel(const char* name, const char* title, TAxis* obs, bool pdfChannel);


  //bbLite means when samples are added into the channels, the channel will 'acquire' the stat factors
  bool addSamples(const char* name, const char* title, const char* channels="*", bool allowNegative=false, bool bbLite=true);

  // A saturating sample is one with an effectively free parameter (a shape factor) in every bin in
  // the channel. During a fit, the saturating sample can explain any data excesses (or deficits
  // if you allow the saturating sample to take on negative values).
  // you can use firstbin and lastbin arguments to restrict which bins are included
  bool addSaturatingSamples(const char* name, const char* title, const char* channels="*", bool
  allowNegative = false, int firstbin = 0, int lastbin = 0);


  TRooHF1* addFactor(const char* name, const char* title, double nomVal=1.);
  TRooHF1* addFactor(const char* name, const char* title, TAxis* ax, double nomVal=1.);
  TRooHF1* addFactor(const char* name, const char* title, const char* observable, int nBins, double min, double max, double nomVal=1.);
  TRooHF1* addFactor(const char* name, const char* title, const char* observable, int nBins, const double* bins, double nomVal=1.);
  TRooHF1* addFactor(const char* name, const char* title, const char* observable, double nomVal=1.);
  // deprecated: use SetBinContent(factor,"",1,val,...) instead
  bool SetFactorContent(const char* name,double val, const char* parName=0, double parVal=0 );
  

    Int_t Fill(const char* sample, const char* channel, double x, double w=1., const char* variationName=0, double variationVal=0);
    bool Fill(const char* sampleName, const char* channelNames, TTree* tree, const char* weight="1", const char* variationName=0, double variationVal=0);

    // shortcuts for filling the data samples (sampleName=0 or "")
    Int_t Fill(const char* channel, double x, double w=1.) { return Fill(0,channel,x,w); }
    bool Fill(const char* channelNames, TTree* tree, const char* weight="1") { return Fill(0,channelNames,tree,weight,0,0); } //fills data!


    bool Add(const char* sample, const char* channel,  TH1* h1, const char* variationName=0, double variationVal=0);
    bool Add(const char* sample, const char* channel, RooAbsReal& arg);






  void SetBinContent(const char* sampleName, const char* channelName, Int_t bin, double val, const char* variationName=0, double variationVal=0);
  void SetBinError(const char* sampleName, const char* channelName, Int_t bin, double val); // sets the statistical error of a component

  double GetBinContent(const char* sampleName, const char* channelName, Int_t bin, const TRooFitResult& fr);
  double GetBinContent(const char* sampleName, const char* channelName, Int_t bin) { return GetBinContent(sampleName,channelName,bin,fCurrentFitResult?*fCurrentFitResult:""); }
  double GetBinContent(const char* channelName, Int_t bin);
  double GetBinError(const char* sampleName, const char* channelName, Int_t bin, const TRooFitResult& fr);
  double GetBinError(const char* sampleName, const char* channelName, Int_t bin) { return GetBinError(sampleName,channelName,bin,fCurrentFitResult?*fCurrentFitResult:""); }
  

  //add a normalization factor to a sample, across all channels
  void Scale(const char* sampleName, const char* channelNames, RooAbsReal& arg);
  void Scale(const char* sampleName,const char* channelNames, const char* par);
  
  //set fill color of sample in given channels (semicolon separated list) (use "*" to specify all channels)
  void SetFillColor(const char* sample, const char* channelNames, Int_t in);
  void SetLineColor(const char* sample, const char* channelNames, Int_t in);
  void SetLineWidth(const char* sample, const char* channelNames, Int_t in);
  
  double IntegralAndError(double& err, const char* sampleName, const char* channelName, const TRooFitResult& res, const char* errGroup=0) const;
  //note: special case for this function, if errGroup is "" then that is interpreted as returning the uncorrelated uncertainties
  double IntegralAndError(double& err, const char* sampleName, const char* channelName, const char* errGroup=0) const { 
    return IntegralAndError(err,sampleName,channelName,((!errGroup || strlen(errGroup)) && fCurrentFitResult)?*fCurrentFitResult:"",errGroup); 
  }
  
  /*double sampleIntegralAndError(double& err, const char* sampleFullName, const TRooFitResult& res="") const;
  double sampleIntegralAndError(double& err, const char* channelName, unsigned int sampleNumber, const TRooFitResult& fr="") const;*/

  // sample can also return 'factors' (samples without channel names)
  TRooAbsH1Fillable* sample(const char* sampleName, const char* channelName=0);
  TRooAbsHStack* channel(const char* name) const;
  TRooHF1* factor(const char* factorName);

  // helper to access components as functions
  //using RooWorkspace::function; -- doesn't seem to get picked up by pyROOT
  RooAbsReal* function(const char* name) const { return RooWorkspace::function(name); }
  RooAbsReal* function(const char* sampleName, const char* channelName) {
      return dynamic_cast<RooAbsReal*>(sample(sampleName,channelName));
  }
  
  void setData(const char* dataName) { 
    if(!data(dataName)) return;
    fCurrentData = dataName; 
  }
  
  using RooWorkspace::data;
  inline RooAbsData* data() { return RooWorkspace::data(fCurrentData); }
  //obtain the global observables associated to a given dataset (this is a snapshot of gobs)
  const RooArgSet* data_gobs(const char* dataName=0) const;
  
  //return the set of parameters that are currently global observables
  const RooArgSet* gobs();
  
  using RooWorkspace::import;
  Bool_t import(std::pair<RooAbsData*,RooArgSet*> dataAndGobs);
  Bool_t import(TRooAbsHStack& channel);
  Bool_t import(TRooHStack& channel) { return import(static_cast<TRooAbsHStack&>(channel)); }
  
  void DisableForcedRecommendedOption(bool in) { kDisabledForcedRecommendedOptions=in; } //use to override forcing of the recommended fit options when calling fitTo
  
  double impact(const char* poi, const char* np, bool positive=true);
  void impact(const char* impactPar=0, float correlationThreshold=0);
  
  //specify _nll to reuse a pre-created nll
  RooFitResult* fitTo(RooAbsData* theData, const RooArgSet* globalObservables=0, Option_t* opt = "hr", RooAbsReal** _nll=0);
  //dataName is name of the internal data to fit to. Opt is fit options (h=hesse, r=reset parameters and remake nll before fitting, q = quiet)
  //fitname is optional name to give to fit when it is saved into the workspace
  RooFitResult* fitTo(const char* dataName=0, Option_t* opt = "hr", const char* fitName = 0);
  RooFitResult* loadFit(const char* fitName,bool prefit=false);
  RooFitResult* getFit(const char* fitName=0) { return dynamic_cast<RooFitResult*>(obj((fitName==0)?fCurrentFit.Data():fitName)); }
  RooAbsReal* getFitNll(const char* fitName=0);
  
  RooAbsReal* nll(const char* nllName) const { return dynamic_cast<RooAbsReal*>(fNll.find(nllName)); }
  
  TGraph* DrawPLL(const char* parName, const char* opt="AL", int nPoints=20);
  
  double pll(const char* poi, RooAbsData* theData, const RooArgSet* globalObservables=0, bool oneSided=false, bool discovery=false);
  double pll(RooArgSet&& poi, RooAbsData* theData, const RooArgSet* globalObservables=0, bool oneSided=false, bool discovery=false);
  double pll(RooArgSet&& _poi, RooAbsData* theData, const RooArgSet* globalObservables, bool (*_compatibilityFunction)(double mu, double mu_hat), RooAbsReal** _nll=0, RooFitResult** _unconditionalFit=0);
  
  double sigma_mu(const char* poi, const char* asimovDataset, double asimovValue=0);
  
  //these functions return the null and alt pvalues from asymptotic formulae
  //_sigma_mu depends on the alt_val, so when you change alt_val you should reset _sigma_mu to 0 if you are using the caching functionality to speed up computations
  std::pair<double,double> asymptoticPValue(const char* poi, RooAbsData* data, const RooArgSet* gobs,bool (*_compatibilityFunction)(double mu, double mu_hat), double alt_val=0, double** _sigma_mu=0, RooFitResult** _unconditionalFit=0);
  std::pair<double,double> asymptoticExpectedPValue(const char* poi, bool (*_compatibilityFunction)(double mu, double mu_hat), double nSigma=0, double alt_val=0, double** _sigma_mu=0);
  
  void addLabel(const char* label) { fLabels.push_back(label); }
  
  TLegend* GetLegend();
  
  double GetSampleCoefficient(const char* sampleFullName) const;
  
  RooSimultaneous* model(const char* channels="*");
  
  bool generateAsimov(const char* name, const char* title, bool fitToObsData=false);
  std::pair<RooAbsData*,RooArgSet*> generateToy(const char* name, const char* title, bool fitToObsData=false, RooAbsPdf::GenSpec** gs=0);
  
  
  //controls which channels are visible when drawing things
  //sets the 'hidden' attribute on non-visible channels
  Int_t SetVisibleChannels(const char* filter) { 
    setChannelAttribute("*","hidden",kTRUE); //hide all channels first
    return setChannelAttribute(filter,"hidden",kFALSE); //unhide the selected ones
  }
  Int_t setChannelStringAttribute(const char* channels,const char* attribute,const char* val);
  Int_t setChannelAttribute(const char* channels,const char* attribute,Bool_t val=kTRUE);
  Int_t setVarAttribute(const char* vars,const char* attribute,Bool_t val=kTRUE);
  
  //draw a channel's stack and overlay the data too
  void channelDraw(const char* channel, Option_t* option="e3005", const TRooFitResult& res = "");
  
  //draws all channels
  virtual void Draw(Option_t* option, const TRooFitResult& res);
  virtual void Draw(Option_t* option="e3005") { 
    //if(fCurrentFit!="") Draw(option,getFit(fCurrentFit));
    if(fCurrentFitResult) Draw(option,*fCurrentFitResult);
    else Draw(option,""); 
  }
  
  //draws all channels, showing how values of channels depend on var
  void DrawDependence(const char* var, Option_t* option="TRI1");
  
  void SetRatioHeight(double in, bool showSignificance=false) { 
    SafeDelete(fLegend);
    fRatioHeight = in; 
    kShowSignificance=showSignificance;
  }
  
  Bool_t writeToFile(const char* fileName, Bool_t recreate=kTRUE);
  
  virtual void Print(Option_t* opt="") const;
  
  void FindVariations(double relThreshold, Option_t* opt = "samples");
  
  static void setDefaultStyle();

  void SetFixedChannelColumns(int in) { fNumChannelColumns = in; }
  
private:
  TString fChannelCatName = "channelCat"; //should only change if wrapping a non-conventional workspace
  TString fSimPdfName = "simPdf"; //should only change if wrapping a non-conventional workspace

  std::map<TString,TH1*> fDummyHists;
  
  TString fCurrentData = "obsData";
  
  TString fCurrentFit = "";
  Bool_t fCurrentFitIsPrefit = false; 
  TRooFitResult* fCurrentFitResult = 0;
  
  //RooArgList fStagedChannels; //channels cannot be added until they are frozen (all content filled)
  
  TLegend* fLegend = 0;

  std::vector<TString> fLabels; //plot labels

  Double_t fRatioHeight = 0; //if nonzero, channelDraw will draw ratio plots

  Bool_t fIsHFWorkspace = false; //if true, this is a histfactory workspace
  Bool_t kDisabledForcedRecommendedOptions = false;
  
  Bool_t kShowSignificance = false;
  int fNumChannelColumns = 0; //! plotting channels, fix number of columns
  
  RooArgList fNll; //!
  
  ClassDef(TRooWorkspace,1) // An extended form of a RooWorkspace
};
 
#endif
