#include "TRooFit/TRooWorkspace.h"

#include "TRooFit/TRooHStack.h"
#include "TRooFit/TRooHPdfStack.h"
#include "TRooFit/TRooH1D.h"

#include "RooCategory.h"
#include "RooDataSet.h"
#include "RooBinning.h"
#include "RooProduct.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TLatex.h"
#include "TStyle.h"
#include "TPRegexp.h"
#include "TMath.h"

#include "TGraphAsymmErrors.h"
#include "RooFactoryWSTool.h"

#include "TRooFit/Utils.h"

#include <algorithm>

#define protected public
#include "RooGaussian.h"
#undef protected

#include "RooUniformBinning.h"

#include "TRegexp.h"


TRooWorkspace::TRooWorkspace(const RooWorkspace& other) : RooWorkspace(other) {
  RooMsgService::instance().getStream(RooFit::INFO).removeTopic(RooFit::NumIntegration); //stop info message every time

  //dataset names do not get correctly imported by copy constructor!!
  auto otherData = other.allData();
  auto myData = allData();
  
  auto otherItr = otherData.begin();
  auto myItr = myData.begin();
  
  while( otherItr != otherData.end() ) {
    (*myItr)->SetName((*otherItr)->GetName());
    (*myItr)->SetTitle(strlen((*otherItr)->GetTitle())==0 ? (*otherItr)->GetName() : (*otherItr)->GetTitle());
    otherItr++;
    myItr++;
  }
  
  //now see if we are a histfactory workspace (i.e. we don't have TRooHStacks defined for all channels ...
  
  //if there is only one RooCategory then assume that is the channelCat name ...
  if(allCats().getSize()==1) fChannelCatName = allCats().first()->GetName();
  
  //similar check for data ...
  if(data(fCurrentData)==0) {
    //use the first data set as the data ...
    if(!allData().empty()) fCurrentData = allData().front()->GetName();
  }
  
  //search for a RooSimultaneous, will use to help create channel stack objects ...
  RooArgSet _allPdfs = allPdfs();
  RooFIter pitr = _allPdfs.fwdIterator();
  RooAbsPdf* _pdf = 0;
  while( (_pdf = dynamic_cast<RooAbsPdf*>(pitr.next())) ) {
    if(_pdf->InheritsFrom(RooSimultaneous::Class())) break;
  }
  if(_pdf && fSimPdfName!=_pdf->GetName()) fSimPdfName=_pdf->GetName();

  // find first dataset featuring the channel attribute, and use that to determine the observables
  if (!set("obs")) {
      RooAbsData* _data = 0;
      for(auto d : allData()) {
          if (!d->get()->find(fChannelCatName)) continue;
          _data = d;
          break;
      }
      if(_data) {
          defineSet("obs", *std::unique_ptr<RooArgSet>(_pdf->getObservables(_data)));
          fCurrentData = _data->GetName();
      } else {
          defineSet("obs", *set("ModelConfig_Observables"));
      }
  }



  
  //create TRooHStack for each channel ...
  TIterator* catIter = cat(fChannelCatName)->typeIterator();
  TObject* c;
  while( (c = catIter->Next()) ) {
    TString cName(c->GetName());
    if(channel(cName)==0) {
      fIsHFWorkspace = true; //is a histfactory model!
    } else {
      if(fDummyHists[cName]==0) {
        fDummyHists[cName] = (TH1*)channel(cName)->GetDummyHist()->Clone(cName);
        fDummyHists[cName]->SetDirectory(0);
      }
      continue; //no need to make a TRooHStack
    }
    

    // commenting out part of next line because thinking we will now always want to
    // run the code below pertaining to externalConstraints of channels
    RooRealSumPdf* channelPdf = 0;//dynamic_cast<RooRealSumPdf*>(pdf(cName+"_model"));
    TRooAbsHStack* channelStack = 0;
    if(!channelPdf) {
      //try getting from the simPdf ...
      if(!_pdf) {
        Error("TRooWorkspace","Could not find model for channel %s",c->GetName());
        channelStack = new TRooHStack(c->GetName(),c->GetName());
      } else {
        RooAbsPdf* myPdf = (dynamic_cast<RooSimultaneous*>(_pdf))->getPdf(cName); //this may be a RooProdPdf (from constraints) ... so we look for a server that is of a known type ...
        
        RooArgSet allNodes; myPdf->treeNodeServerList(&allNodes);
        RooFIter nodeItr = allNodes.fwdIterator();
        RooAbsArg* arg = 0;
        while( (arg = nodeItr.next()) ) {
          
          if(arg->InheritsFrom(RooRealSumPdf::Class())) {
            channelStack = new TRooHStack(*dynamic_cast<RooRealSumPdf*>(arg),*set("obs"));
            dynamic_cast<RooAbsReal*>(channelStack)->SetName(c->GetName());dynamic_cast<RooAbsReal*>(channelStack)->SetTitle(c->GetName());
            channelStack->SetMinimum(1e-9);
            dynamic_cast<RooAbsReal*>(channelStack)->setStringAttribute("originalObject",arg->GetName());
            break;
          } else if(arg->InheritsFrom(RooAddPdf::Class())) {
            channelStack = new TRooHPdfStack(*dynamic_cast<RooAddPdf*>(arg),*set("obs"));
            dynamic_cast<RooAbsReal*>(channelStack)->SetName(c->GetName());dynamic_cast<RooAbsReal*>(channelStack)->SetTitle(c->GetName());
            channelStack->SetMinimum(1e-9);
            dynamic_cast<RooAbsReal*>(channelStack)->setStringAttribute("originalObject",arg->GetName());
            break;
          }
        }
        if(!channelStack) {
          Error("TRooWorkspace","Could not identify model for channel %s",c->GetName());
          channelStack = new TRooHStack(c->GetName(),c->GetName());
        }

        if (myPdf->InheritsFrom(RooProdPdf::Class())) {
            // take any pdf that isn't the channelStack pdf as an externalConstraint for that
            // channel
            RooAbsArg* cPdf = arg; // will be 0 if couldnt find a channel pdf
            auto prodPdf = dynamic_cast<RooProdPdf*>(myPdf);
            RooArgSet constraints;
            for(int i=0;i<prodPdf->pdfList().getSize();i++) {
                if(prodPdf->pdfList().at(i) == cPdf) {
                    continue;
                }
                constraints.add(*prodPdf->pdfList().at(i));
            }
            if (constraints.getSize()) {
                defineSet(Form("constraints_%s",c->GetName()), constraints);
                dynamic_cast<RooAbsArg*>(channelStack)->setStringAttribute("externalConstraints",
                        Form("constraints_%s",c->GetName()));
            }
        }

      }
    } else {
      channelStack = new TRooHStack(*channelPdf,*set("obs"));
      dynamic_cast<RooAbsReal*>(channelStack)->SetName(c->GetName());dynamic_cast<RooAbsReal*>(channelStack)->SetTitle(c->GetName());
      channelStack->SetMinimum(1e-9);
      dynamic_cast<RooAbsReal*>(channelStack)->setStringAttribute("originalObject",channelPdf->GetName());
    }
    import(*dynamic_cast<RooAbsPdf*>(channelStack),RooFit::Silence());
    
    //update the titles of components in the stack so that they are more 'human' ....
    
    RooFIter funcIter = channelStack->compList().fwdIterator() ;
    RooFIter coefIter = channelStack->coeffList().fwdIterator() ;
    RooAbsReal* func ;
    RooAbsReal* coef ;
  
    //obtain a list of the coeff and func names ... determine common starts and ends, and add these to a list - will use these to truncate hist titles ...
    std::vector<TString> titles;
    while((func=(RooAbsReal*)funcIter.next())) {
      coef = (RooAbsReal*)coefIter.next();
      titles.emplace_back(Form("%s_x_%s",coef->GetTitle(),func->GetTitle()));
    }
    
    bool nameInComps=true;
    for(auto& s : titles) {
      if(!s.Contains(channelStack->GetName())) { nameInComps=false;break; }
    }
    
    
    TString commonStart="";TString commonEnd="";
    if(titles.size()>1) {
      bool stillValid=true;
      while(stillValid && commonStart.Length() < titles[0].Length()) {
        commonStart = titles[0](0,commonStart.Length()+1);
        for(auto& s : titles) {
          if(s(0,commonStart.Length()) != commonStart) { stillValid=false; commonStart = commonStart(0,commonStart.Length()-1); break; }
        }
      }
      stillValid=true;
      while(stillValid && commonEnd.Length() < titles[0].Length()) {
        commonEnd = titles[0](titles[0].Length()-commonEnd.Length()-1,commonEnd.Length()+1);
        for(auto& s : titles) {
          if(s(s.Length()-commonEnd.Length(),commonEnd.Length()) != commonEnd) { stillValid=false; commonEnd = commonEnd(1,commonEnd.Length()-1); break; }
        }
      }
    }
    
    funcIter = channelStack->compList().fwdIterator() ;
    coefIter = channelStack->coeffList().fwdIterator() ;
    
    //for nicer histfactory support ...
    while((func=(RooAbsReal*)funcIter.next())) {
        coef = (RooAbsReal*)coefIter.next();
        TString myTitle(func->GetTitle());//Form("%s_x_%s",coef->GetTitle(),func->GetTitle())); NOTE -- higgs comb workspaces often have the useful part in the coef name ... hmm :-/

        if (myTitle.Contains(TRegexp(TString::Format("L_x_.*_%s_.*",channelStack->GetTitle()) ))) {
            myTitle = myTitle(4, myTitle.Index(channelStack->GetTitle())-5);
        } else {
            if (myTitle.Contains(TString("_") + channelStack->GetTitle()))
                myTitle = myTitle.ReplaceAll(TString("_") + channelStack->GetTitle(), "");
            myTitle.ReplaceAll("L_x_", "");
            myTitle.ReplaceAll("_overallSyst", "");
            myTitle.ReplaceAll("_x_StatUncert", "");
            myTitle.ReplaceAll("_x_HistSyst", "");
            myTitle.ReplaceAll("_x_Exp", "");
            myTitle.ReplaceAll(commonStart,""); myTitle.ReplaceAll(commonEnd,"");
            if(nameInComps) myTitle.ReplaceAll(channelStack->GetName(),"");
        }
        func->SetTitle(myTitle); //update the title
    }
    
    
    
    //create a dummy hist for each channel too
    fDummyHists[c->GetName()] = (channelStack->fDummyHist) ? (TH1*)channelStack->fDummyHist->Clone(c->GetName()) : new TH1D(c->GetName(),c->GetName(),1,0,1);
    // need to give this hist the name of the observable
    fDummyHists[c->GetName()]->SetName( channelStack->GetObservableName(0) );

    fDummyHists[c->GetName()]->SetDirectory(0);
    delete channelStack; //since channelStack was cloned when it was imported
  }
  
  if(fIsHFWorkspace) {
      RooStats::ModelConfig* mc = 0;
      for(auto o : allGenericObjects()) {
          mc = dynamic_cast<RooStats::ModelConfig*>(o);
          if (mc) break;
      }
      const RooArgSet* np = nullptr;
      if(mc) {
          np = mc->GetNuisanceParameters();
          if (mc->GetParametersOfInterest()) defineSet("poi",*mc->GetParametersOfInterest());
          if (mc->GetGlobalObservables()) {
              if(!set("globalObservables")) defineSet("globalObservables",*mc->GetGlobalObservables());
              (const_cast<RooArgSet*>(set("globalObservables")))->setName("globalObservables");
          }
      }
    
    
     auto _allVars = dynamic_cast<RooArgSet*>(allVars().selectByAttrib("Constant",kFALSE));
     _allVars->remove(*(const_cast<TRooWorkspace*>(this)->set("obs")));
     RooArgSet _allPdfs = allPdfs();
    
    //go through nuisance parameters and  guess errors for any params that dont have errors ..
    const RooArgSet* nomVals = getSnapshot("NominalParamValues");
    if(np) {
      RooFIter itr = np->fwdIterator();
      while(RooAbsArg* arg = itr.next()) {
        auto v = dynamic_cast<RooRealVar*>(arg);
        if(!v) continue;
        //while here, tweak the title of the parameter if it matches the name ...
        if(strcmp(v->GetName(),v->GetTitle())==0) {
          TString t(v->GetTitle());
          //only replace up to one char because of some ROOT issue displaying binLabels with more than 1 greek symbol
          if(!t.Contains('#')) t.ReplaceAll("alpha","#alpha");
          if(!t.Contains('#')) t.ReplaceAll("gamma","#gamma");
          if(!t.Contains('#')) t.ReplaceAll("mu","#mu");
          v->SetTitle(t);
        }
        if(v->getError()) continue; //already has an error 
        if(v->isConstant()) continue; //shouldn't be the case 
        //HistFactory models have alpha parameters with range -5 to 5 ... error should be 1
        if(TString(v->GetName()).BeginsWith("alpha_") && fabs(v->getMin()+5.)<1e-9 && fabs(v->getMax()-5.)<1e-9) {
          //Info("TRooWorkspace","Guessing nominal error of %s is 1",v->GetName());
          v->setError(1);
          //propagate to NominalParamValues too, if there ...
          if(nomVals && nomVals->find(v->GetName())) (dynamic_cast<RooRealVar*>(nomVals->find(v->GetName())))->setError(v->getError());
          v->setStringAttribute("constraintType","EXTERNAL");
        } else if(TString(v->GetName()).BeginsWith("gamma_stat_") && fabs(v->getMin())<1e-9) {
          //stat parameters have their max value set to 5*sigma ...
          v->setError( (v->getMax()-1.)/5. );
          if(nomVals && nomVals->find(v->GetName())) (dynamic_cast<RooRealVar*>(nomVals->find(v->GetName())))->setError(v->getError());
          v->setStringAttribute("constraintType","EXTERNAL");
        } else {
          RooRealVar* _var = v;
          //check for constraint term .. which is a pdf featuring just this parameter
          RooArgSet otherPars(*_allVars); otherPars.remove(*_var);
          RooFIter pitr = _allPdfs.fwdIterator();
          RooAbsPdf* constraintPdf = 0;
          while( RooAbsPdf* _pdf = dynamic_cast<RooAbsPdf*>(pitr.next()) ) {
            if(!_pdf->dependsOn(*_var)) continue;
            if(_pdf->dependsOn(otherPars)) continue;
            constraintPdf = _pdf; break;
          }
          if(constraintPdf) {
            if(constraintPdf->InheritsFrom("RooGaussian")) {
              //determine the "mean" (usually a global observable) and the standard deviation ...
              RooGaussian* gPdf = static_cast<RooGaussian*>(constraintPdf);
              
              double sigma= gPdf->sigma;
              _var->setError(sigma);
              if(nomVals && nomVals->find(v->GetName())) (dynamic_cast<RooRealVar*>(nomVals->find(v->GetName())))->setError(v->getError());
            } else if(constraintPdf->InheritsFrom("RooPoisson")) {
              //assume that _var appears multiplicatively inside the mean of the poisson ... so find the server that depends on _var and getVal and divide by _var val to determine tau factor
              //tau factor is square of inverse relatively uncert ... 
              RooFIter sItr = constraintPdf->serverMIterator();
              RooAbsReal* ss;
              while( (ss = (RooAbsReal*)sItr.next()) ) {
                if(ss->dependsOn(*_var)) break;
              }
              if(ss) {
                double tau = ss->getVal() / _var->getVal();
                _var->setError( 1./sqrt(tau) );
                if(nomVals && nomVals->find(v->GetName())) (dynamic_cast<RooRealVar*>(nomVals->find(v->GetName())))->setError(v->getError());
              } 
            } 
          }
        }
        
      }
    }
    delete _allVars;
  }
  
  if(!set("obs")) {
    defineSet("obs",*data(fCurrentData)->get());
  }
  
  if(!set("globalObservables")) {
    //if got here then need to create the global observables snapshot too ...
    //need list of gobs then ..
    RooAbsPdf* m = pdf(fSimPdfName);
     //infer the global observables, nuisance parameters, model args (const np) 
    RooArgSet* gobs_and_np = m->getParameters(*set("obs"));

    //remove the poi ...
    //if(set("poi")) gobs_and_np->remove(*set("poi"));

    RooArgSet _gobs;
    _gobs.add(*gobs_and_np); //will remove the np in a moment

    //now pass this to the getAllConstraints method ... it will modify it to contain only the np!
    RooArgSet* s = m->getAllConstraints(*set("obs"),*gobs_and_np);
     delete s; //don't ever need this 

     //gobs_and_np now only contains the np
     _gobs.remove(*gobs_and_np);
     //ensure all global observables are held constant now - important for pll evaluation
     _gobs.setAttribAll("Constant",kTRUE);
     
     defineSet("globalObservables",_gobs);
     
     
     
  }
  
  for(auto& d : allData()) {
    //ensure there's a gobs set for every data ...
    if(!set(Form("%s_globalObservables",d->GetName()))) saveSnapshot(Form("%s_globalObservables",d->GetName()),*set("globalObservables"));
  }
  
  
}

TRooHF1* TRooWorkspace::addFactor(const char* name, const char* title, double nomVal) {
  TRooHF1 _factor(name,title,RooArgList(),{},{});
  _factor.SetBinContent(1,nomVal);
  import(_factor);
  return factor(name);
}

TRooHF1* TRooWorkspace::addFactor(const char* name, const char* title, TAxis* ax, double nomVal) {
    RooRealVar* v = var(ax->GetName());
    if(!v) {
        v = addVariable(*ax);
        if(!v) return 0;
    }
    if (ax->IsVariableBinSize()) {
        TRooHF1 _factor(name, title, RooArgList(*v), {ax->GetNbins()}, {ax->GetXbins()->GetArray()});
        for(int i=1;i<=_factor.GetNbinsX();i++) _factor.SetBinContent(i,nomVal);
        import(_factor);
    } else {
        TRooHF1 _factor(name, title, RooArgList(*v), {ax->GetNbins()}, {ax->GetXmin()},{ax->GetXmax()});
        for(int i=1;i<=_factor.GetNbinsX();i++) _factor.SetBinContent(i,nomVal);
        import(_factor);
    }
    return factor(name);
}

TRooHF1* TRooWorkspace::addFactor(const char* name, const char* title, const char* observable, int nBins, double min, double max, double nomVal) {
  RooRealVar* v = var(observable);
  if(!v) {
    v = addObservable(observable,observable,nBins,min,max);
    if(!v) return 0;
  }
  TRooHF1 _factor(name,title,RooArgList(*v),{nBins},{min},{max});
  for(int i=1;i<=_factor.GetNbinsX();i++) _factor.SetBinContent(i,nomVal);
  import(_factor);
  return factor(name);
}

TRooHF1* TRooWorkspace::addFactor(const char* name, const char* title, const char* observable, int nBins, const double* bins, double nomVal) {
  RooRealVar* v = var(observable);
  if(!v) {
    v = addObservable(observable,observable,nBins,bins);
    if(!v) return 0;
  }
  TRooHF1 _factor(name,title,RooArgList(*v),{nBins},{bins});
  for(int i=1;i<=_factor.GetNbinsX();i++) _factor.SetBinContent(i,nomVal);
  import(_factor);
  return factor(name);
}

TRooHF1* TRooWorkspace::addFactor(const char* name, const char* title, const char* observable, double nomVal) {
  RooRealVar* v = var(observable);
  if(!v) return 0;
  if(v->getBinning(name).isUniform()) {
    return addFactor(name,title,observable,v->getBins(name),v->getMin(name),v->getMax(name),nomVal);
  } else {
    return addFactor(name,title,observable,v->getBins(name),v->getBinning(name).array(),nomVal);
  }
}

TRooHF1* TRooWorkspace::factor(const char* factorName) {
  return dynamic_cast<TRooHF1*>(function(factorName));
}

bool TRooWorkspace::SetFactorContent(const char* name, double val,const char* parName, double parVal) {
  TRooHF1* _factor = factor(name);
  if(!_factor) return false;
  
  if(parName && !var(parName)) {
    Info("factorSetVariation","%s is not defined. Creating a gaussian-constrained parameter",parName);
    addParameter(parName,parName,0,-5,5,"normal");
  }
  _factor->SetBinContent(1, val, (parName) ? var(parName) : 0, parVal);
  return true;
}




RooRealVar* TRooWorkspace::addVariable(const TAxis& ax) {
    TString name = ax.GetName();
    TString binning = "";
    if (name.Contains(';')) {
        // part after ; is the binning name
        binning = name(name.Index(';')+1,name.Length());
        name = name(0,name.Index(';'));
    }
    int type = 0; // observable default
    double val = 0;
    if (std::isinf( ax.GetXmax() ) || std::isinf( ax.GetXmin()) ) {
        type = 2; // argument
        // initial value will be the non-infinite limit, or 0 if both are infinite
        if (!std::isinf( ax.GetXmax() )) {
            val = ax.GetXmax();
        } else if (!std::isinf( ax.GetXmin() )) {
            val = ax.GetXmin();
        }
    } else if (ax.GetNbins() == 0) {
        type = 1; // parameter
        val = (ax.GetXmax() + ax.GetXmin()) / 2;
    } else {
        // observable, default value in center of range
        val = (ax.GetXmax() + ax.GetXmin()) / 2;
    }

    RooRealVar* v = var(name);
    if (!v) {
        // creating variable
        if (type == 2) {
            factory(Form("%s[%f]",name.Data(),val));
        } else {
            factory(Form("%s[%f,%f,%f]",name.Data(),val,ax.GetXmin(),ax.GetXmax()));
        }
        v = var(name);
        if (type == 2 || type == 1) {
            // no binning for parameters and arguments
            v->setBins(0);
            v->setConstant(type == 2);
        } else {
            if (ax.IsVariableBinSize()) {
                v->setBinning(RooBinning(ax.GetNbins(), ax.GetXbins()->GetArray()));
            } else {
                v->setBins(ax.GetNbins());
            }
            v->getBinning().SetTitle(ax.GetTitle());
        }
        TString title = ax.GetTitle();
        if (title.Contains('/')) {
            // use part after / as the unit
            v->setUnit(title(title.Index('/')+1,title.Length()).Data());
            title = title(0, title.Index('/'));
        }
        v->SetTitle(ax.GetTitle());

        if (type == 0) {
            if(!set("obs")) defineSet("obs",name);
            else extendSet("obs",name);
        }
    } else {
        // set range or binning if a binning is given and not defined
        if (binning.Length() > 0 && !v->hasRange(binning)) {
            if (type == 0) {
                if (ax.IsVariableBinSize()) {
                    v->setBinning(RooBinning(ax.GetNbins(), ax.GetXbins()->GetArray()),binning);
                } else {
                    v->setBinning(RooUniformBinning(ax.GetXmin(),ax.GetXmax(),ax.GetNbins()),binning);
                }
                v->getBinning().SetTitle(ax.GetTitle());
            } else {
                v->setRange(binning, ax.GetXmin(),ax.GetXmax());
            }
            v->getBinning(binning).SetTitle(ax.GetTitle());
        }
    }

    // ensure the nominal range of v covers the axis
    // if this isn't done, calls to GetBinContent on TRooH1 wont work outside
    // nominal range (setBin hits the limits)
    if (v->getMin() > ax.GetXmin()) v->setMin(ax.GetXmin());
    if (v->getMax() < ax.GetXmax()) v->setMax(ax.GetXmax());

    // ensure range in datasets is also extended, otherwise errors when run fit
    for(auto d : allData()) {
        auto cc = dynamic_cast<RooRealVar*>(d->get()->find(*v));
        if (cc) {
            if (cc->getMin() > ax.GetXmin()) cc->setMin(ax.GetXmin());
            if (cc->getMax() < ax.GetXmax()) cc->setMax(ax.GetXmax());
        }
    }

    return v;
}

RooRealVar* TRooWorkspace::addParameter(const char* name, const char* title, double val, double min, double max, const char* constraintType) {
    TAxis ax(0,min,max);ax.SetNameTitle(name,title);
    auto v = addVariable(ax);
    v->setVal(val);
    if(constraintType) {
        v->setStringAttribute("constraintType",constraintType);
        TString cType(constraintType);
        cType.ToUpper();
        if(cType=="NORMAL") v->setError(1);
    }
    return v;
}

RooRealVar* TRooWorkspace::addParameter(const char* name, const char* title, double min, double max, const char* constraintType) {
    return addParameter(name,title,(min+max)/2,min,max,constraintType);
}

RooRealVar* TRooWorkspace::addObservable(const char* name, const char* title, double min, double max) {
    TAxis ax(1,min,max);ax.SetNameTitle(name,title);
    return addVariable(ax);
}

RooRealVar* TRooWorkspace::addObservable(const char* name, const char* title, int nBins, double min, double max) {
    TAxis ax(nBins,min,max);ax.SetNameTitle(name,title);
    return addVariable(ax);
}

RooRealVar* TRooWorkspace::addObservable(const char* name, const char* title, int nBins, const double* bins) {
    TAxis ax(nBins,bins);ax.SetNameTitle(name,title);
    return addVariable(ax);
}


RooRealVar* TRooWorkspace::addArgument(const char* name, const char* title, double val) {
  TAxis ax(0,val,INFINITY);ax.SetNameTitle(name,title);
  return addVariable(ax);
}


//creating a channel by importing an existing channel ...
Bool_t TRooWorkspace::import(TRooAbsHStack& channel) {
  
  RooRealVar* obs = var(channel.GetObservableName(0));
  
  RooCategory* c = cat(fChannelCatName);
  if(!c) {
    factory().createCategory(fChannelCatName,channel.GetName());
    c = cat(fChannelCatName);
    if(!set("obs")) defineSet("obs",fChannelCatName);
    else extendSet("obs",fChannelCatName);
    
    if(!c) return 0;
  }
  else {
    c->defineType(channel.GetName());
  }
  
  TH1* dummyHist = (TH1*)channel.GetDummyHist()->Clone(channel.GetObservableName(0));
  dummyHist->SetDirectory(0);

  
  bool out =  RooWorkspace::import(dynamic_cast<RooAbsReal&>(channel));

    dummyHist->SetTitle("Data");
    dummyHist->GetXaxis()->SetTitle(var(channel.GetObservableName(0))->GetTitle()); //ensure obs name is correct

    fDummyHists[channel.GetName()] = dummyHist;

  if(!obs) {
    if(!set("obs")) defineSet("obs",channel.GetObservableName(0));
    else extendSet("obs",channel.GetObservableName(0));
  }
  
  return out;
}


TRooHStack* TRooWorkspace::addChannel(const char* name, const char* title, const char* observable, int nBins, double min, double max) {
    TAxis x(nBins,min,max);x.SetName(observable);
    x.SetTitle(var(observable)? var(observable)->GetTitle() : observable);
    return dynamic_cast<TRooHStack*>(addChannel(name,title,&x,false));
}

TRooHStack* TRooWorkspace::addChannel(const char* name, const char* title, const char* observable, int nBins, const double* bins) {
  TAxis x(nBins,bins);x.SetName(observable);
  x.SetTitle(var(observable)? var(observable)->GetTitle() : observable);
  return dynamic_cast<TRooHStack*>(addChannel(name,title,&x,false));
}

TRooHStack* TRooWorkspace::addChannel(const char* name, const char* title, const char* observable) {
  RooRealVar* v = var(observable);
  if(!v) return 0;
  if(v->getBinning(name).isUniform()) {
    return addChannel(name,title,observable,v->getBins(name),v->getMin(name),v->getMax(name));
  } else {
    return addChannel(name,title,observable,v->getBins(name),v->getBinning(name).array());
  }
}

TRooAbsHStack* TRooWorkspace::addChannel(const char* name, const char* title, TAxis* obs, bool
pdfChannel) {
    // get or create the variable using the TAxis ... creates a binning with name = channel name
    TString obs_name = obs->GetName();
    if (!obs_name.Contains(';')) {
        obs->SetName(TString(obs->GetName()) + ";" + name);
    }
    RooRealVar* v = addVariable(*obs);

    RooCategory* c = cat(fChannelCatName);
    if(!c) {
        factory().createCategory(fChannelCatName,name);
        c = cat(fChannelCatName);
        if(!set("obs")) defineSet("obs",fChannelCatName);
        else extendSet("obs",fChannelCatName);

        if(!c) return 0;
    } else {
        // define type, must all add to any existing datasets
        c->defineType(name);
        for(auto d : allData()) {
            auto cc = dynamic_cast<RooCategory*>(d->get()->find("channelCat"));
            if (cc) {
                cc->defineType(name);
            }
        }
    }

    TRooAbsHStack* hs = 0;

    if (!pdfChannel) {
        hs = new TRooHStack(name, title);
    } else {
        hs = new TRooHPdfStack(name,title);
    }
    hs->setFloor(true,1e-9); //this also does the SetMinimim
    auto hstmp = hs;
    RooWorkspace::import(*dynamic_cast<RooAbsArg*>(hs));
    hs = dynamic_cast<TRooAbsHStack*>(function(hs->GetName())); //FIXME should check it's ok
    delete hstmp;

    //need to store a dummyHist for the binning ...
    if(v->getBinning(name).isUniform()) {
        fDummyHists[name] = new TH1D(v->GetName(),"Data",v->getBins(name),v->getMin(name),v->getMax(name));
    } else {
        fDummyHists[name] = new TH1D(v->GetName(),"Data",v->getBins(name),v->getBinning(name).array());
    }
    fDummyHists[name]->SetDirectory(0);
    fDummyHists[name]->GetXaxis()->SetName(v->GetName());
    RooArgList _obs; _obs.add(*v);
    hs->SetDummyHist(fDummyHists[name], &_obs);

    return hs;
}



Int_t TRooWorkspace::setChannelAttribute(const char* channels,const char* attribute,Bool_t val) {
    std::vector<TRegexp> patterns;
    TStringToken pattern(channels,";");
    while(pattern.NextToken()) {
        patterns.emplace_back( TRegexp(pattern,true) );
    }

  TIterator* catIter = cat(fChannelCatName)->typeIterator();
  int nCat(0);
  TObject* c;
  while( (c = catIter->Next()) ) {
    TString cName(c->GetName());
    bool found=false;
    for(auto& p : patterns) {
        if (cName.Contains(p)) {found=true;break;}
    }
    if(found) {
      function(c->GetName())->setAttribute(attribute,val);
      //propagate to original object (e.g. original RooRealSumPdf) if necessary
      if(function(c->GetName())->getStringAttribute("originalObject")) function( function(c->GetName())->getStringAttribute("originalObject") )->setAttribute(attribute,val);
      nCat++;
    } 
  }
  delete catIter;
  return nCat;
}

Int_t TRooWorkspace::setChannelStringAttribute(const char* channels,const char* attribute,const char* val) {
    std::vector<TRegexp> patterns;
    TStringToken pattern(channels,";");
    while(pattern.NextToken()) {
        patterns.emplace_back( TRegexp(pattern,true) );
    }

    TIterator* catIter = cat(fChannelCatName)->typeIterator();
    int nCat(0);
    TObject* c;
    while( (c = catIter->Next()) ) {
        TString cName(c->GetName());
        bool found=false;
        for(auto& p : patterns) {
            if (cName.Contains(p)) {found=true;break;}
        }
        if(found) {
      function(c->GetName())->setStringAttribute(attribute,val);
      //propagate to original object (e.g. original RooRealSumPdf) if necessary
      if(function(c->GetName())->getStringAttribute("originalObject")) function( function(c->GetName())->getStringAttribute("originalObject") )->setStringAttribute(attribute,val);
      nCat++;
    } 
  }
  delete catIter;
  return nCat;
}

Int_t TRooWorkspace::setVarAttribute(const char* channels,const char* attribute,Bool_t val) {
  TRegexp pattern(channels,true);
  RooArgSet _allVars = allVars();
  RooFIter itr = _allVars.fwdIterator();
  RooAbsArg* c;
  int nCat(0);
  while( (c = itr.next()) ) {
    TString cName(c->GetName());
    if(cName.Contains(pattern)) {
      c->setAttribute(attribute,val);
      nCat++;
    } 
  }
  return nCat;
}

bool TRooWorkspace::addSamples(const char* name, const char* title, const char* channels, bool allowNegative, bool bbLite) {

  std::vector<TRegexp> patterns;
  TStringToken pattern(channels,";");
  while(pattern.NextToken()) {
    patterns.emplace_back( TRegexp(pattern,true) );
  }
  
  
  //loop over channels, create sample for each one that matches pattern
  TIterator* catIter = cat(fChannelCatName)->typeIterator();
  TObject* c;
  while( (c = catIter->Next()) ) {
    TString cName(c->GetName());
    bool matched(false);
    for(auto& p : patterns) if(cName.Contains(p)) { matched=true; break; }
    if(!matched) continue;

    fDummyHists[c->GetName()]->Reset(); //ensures it is empty
    TString hName(Form("%s_%s",name,c->GetName()));
    TRooH1D* h = new TRooH1D(hName,title,*var(fDummyHists[c->GetName()]->GetName()),fDummyHists[c->GetName()]);
    h->SetFillColor( TRooFit::GetColorByName(name,true) );
    if(!allowNegative) h->setFloor(true,0); //by default, samples cannot go negative
    TRooH1D* htmp = h;
    import(*h);h = static_cast<TRooH1D*>(function(h->GetName()));delete htmp;
    channel(c->GetName())->Add(h, bbLite);

  }
  
  delete catIter;
  
  return true;
  
}


#include "TCut.h"


bool TRooWorkspace::Fill(const char* sampleName, const char* channelNames, TTree* tree, const char* weight, const char* variationName, double variationVal) {

  std::vector<TRegexp> patterns;
  TStringToken pattern(channelNames,";");
  while(pattern.NextToken()) {
    patterns.emplace_back( TRegexp(pattern,true) );
  }
  
  
  //loop over channels, create sample for each one that matches pattern
  std::unique_ptr<TIterator> catIter(cat(fChannelCatName)->typeIterator());
  TObject* c;
  while( (c = catIter->Next()) ) {
    TString cName(c->GetName());
    bool matched(false);
    for(auto& p : patterns) if(cName.Contains(p)) { matched=true; break; }
    if(!matched) continue;

    // if the observable has a formula, use that, otherwise use the observables name
    TString formula(fDummyHists[cName]->GetName());
    if( var( fDummyHists[cName]->GetName() )->getStringAttribute("formula") ) {
        formula = var( fDummyHists[cName]->GetName() )->getStringAttribute("formula");
    }

    // TODO: Check formula compiles

    RooAbsReal* s = 0;
    if(sampleName) { //no sampleName means will get interpreted as adding data
      s = dynamic_cast<RooAbsReal*>(sample(sampleName,cName));
      if(!s) continue; //skip channel because sample does not exist
    }
    TRooAbsHStack* cc = channel(cName);
    RooAbsReal* ccFunc = dynamic_cast<RooAbsReal*>(cc);
  
    TH1* histToFill = (TH1*)fDummyHists[cName]->Clone("tmpHist");
    histToFill->Reset();


    // cut will be channel_cut * sample_cut * weight
    // if channel_cut or sample_cut not present, it is just 1

    //can use either 'cuts' or 'formula' for this feature
    TCut channel_cut("channel_cut", "1");
    if (ccFunc->getStringAttribute("cuts")) {
        channel_cut.SetTitle(ccFunc->getStringAttribute("cuts"));
    } else if(ccFunc->getStringAttribute("formula")) {
        channel_cut.SetTitle(ccFunc->getStringAttribute("formula"));
    }
    TCut sample_cut("sample_cut", "1");
    if(s && s->getStringAttribute("cuts")) {
        sample_cut.SetTitle(s->getStringAttribute("cuts"));
    } else if(s && s->getStringAttribute("formula")) {
        sample_cut.SetTitle(s->getStringAttribute("formula"));
    }
    TCut weight_cut("weight",weight);


    tree->Draw(Form("%s>>tmpHist",formula.Data()),channel_cut*sample_cut*weight_cut,"goff");
    Add(sampleName,cName,histToFill,variationName,variationVal);
    delete histToFill;
  }
  return true;
}

Int_t TRooWorkspace::Fill(const char* sampleName, const char* channelName,  double x, double w, const char* variationName, double variationVal) {
  if(variationName && !var(variationName)) {
    Info("Fill","%s is not defined. Creating a gaussian-constrained parameter",variationName);
    addParameter(variationName,variationName,0,-5,5,"normal");
  }

  if (!sampleName || strlen(sampleName)==0 || data(sampleName)) {
      if (variationName && strlen(variationName)) {
          Error("Fill","Variations are not allowed for data");
          return 0;
      }
      TString _dataName = (!sampleName || strlen(sampleName)==0) ? fCurrentData.Data() : sampleName;
      // filling data ...
      if(!data(_dataName) ) {
          RooArgSet s(*set("obs"));
          RooRealVar wv("weightVar","weightVar",1);
          s.add(wv);
          RooDataSet* _data = new RooDataSet("obsData","obsData",s,"weightVar");
          import(*_data);
          delete _data;
          ((RooDataSet*)data())->removeFromDir(data()); // is this safe?
      }

      cat(fChannelCatName)->setLabel(channelName);
      var( fDummyHists[channelName]->GetName() )->setVal( x );
      data(_dataName)->add(*set("obs"),w);
      return 1;
  }

  return sample(sampleName,channelName)->Fill( x, w, variationName ? var(variationName) : 0, variationVal);
}

bool TRooWorkspace::Add(const char* sampleName, const char* channelName,  TH1* h1,const char* variationName, double variationVal) {
    TString s = (variationName) ? TString::Format("%s;%s;%s=%f",sampleName,channelName,
            variationName,variationVal) : TString::Format("%s;%s",sampleName,channelName);
    h1->SetName(s);
    auto chan =  channel(channelName);
    if(chan) {
        h1->GetXaxis()->SetName( chan->GetObservableName(0));
    } else if (factor(sampleName)) {
        h1->GetXaxis()->SetName(factor(sampleName)->GetObservableName(0));
    }
    return Add(*h1);
}

bool TRooWorkspace::Add(const char* sampleName, const char* channelName,  RooAbsReal& arg) {
    if (sample(sampleName,channelName)) {
        return sample(sampleName,channelName)->Add(arg);
    }

    if (channel(channelName)) {
        // sample doesn't exist, use the arg as the sample
        if (!function(arg.GetName())) {
            // import it
            import(arg);
        }
        channel(channelName)->Add(function(arg.GetName()));
        return true;
    }

    return false;

}


void TRooWorkspace::SetBinContent(const char* sampleName, const char* channelName, Int_t bin, double val, const char* parName, double parVal) {
    auto samp = sample(sampleName,channelName);
    if (!samp) {
        // see if this is actually data ...
        if (!sampleName || strlen(sampleName)==0 || data(sampleName)) {
            // need to remove entries in this bin from the sample
            if (fDummyHists.find(channelName) == fDummyHists.end()) return;

            TString x_var = fDummyHists[channelName]->GetXaxis()->GetName();

            TString _dataName = (!sampleName || strlen(sampleName)==0) ? fCurrentData.Data() : sampleName;
            auto _data = data(_dataName);
            if (_data) {
                auto cut = TString::Format("%s==%s::%s && %s >= %f && %s < %f",fChannelCatName.Data(),fChannelCatName.Data(),channelName, x_var.Data(),
                                           fDummyHists[channelName]->GetBinLowEdge(bin), x_var.Data(),
                                           fDummyHists[channelName]->GetBinLowEdge(bin + 1));

                if (_data->sumEntries(cut) > 0) {
                    auto _reduced = _data->reduce(TString::Format("!(%s)",cut.Data()));
                    _data->reset();
                    for(size_t i = 0; i <_reduced->numEntries();i++) {
                        auto _obs = _reduced->get(i);
                        _data->add(*_obs, _reduced->weight());
                    }
                }
            }
            Fill(sampleName,channelName,fDummyHists[channelName]->GetBinCenter(bin), val);
            return;
        }
        // create the sample
        addSamples(sampleName,sampleName,channelName);
        samp = sample(sampleName,channelName);
        if (!samp) {
            return;
        }
    }

    //get the parameter
    if(parName && !var(parName)) {
        Info("SetBinContent","%s is not defined. Creating a gaussian-constrained parameter",parName);
        addParameter(parName,parName,0,-5,5,"normal");
    }
    samp->SetBinContent(bin,val, parName ? var(parName) : 0, parVal);
}

void TRooWorkspace::SetBinError(const char* sampleName, const char* channelName, Int_t bin, double val) {
    sample(sampleName,channelName)->SetBinError(bin,val);
}

double TRooWorkspace::GetBinError(const char* sampleName, const char* channelName, Int_t bin, const TRooFitResult& fr) {
  auto chan = channel(channelName);
  if(!chan) {
    Error("GetBinError","Unrecognised channel: %s",channelName);
    return 0;
  }

    int sampleNumber = -1;
    RooAbsReal* samp = 0;
    if(sampleName==0 || strlen(sampleName)==0 || data(sampleName)) {
        // getting a data yield ...
        Error("GetBinError","No error on data");
        return 0;
    } else if(strcmp(sampleName,channelName)==0) {
        samp = dynamic_cast<RooAbsReal *>(chan);
    } else {
        sampleNumber = chan->compList().index(Form("%s_%s",sampleName,channelName));
        if(sampleNumber==-1) sampleNumber = chan->compList().index(sampleName);
        if(sampleNumber==-1) return 0;
        samp = dynamic_cast<RooAbsReal*>(chan->compList().at(sampleNumber));
    }
  
  if(!samp) {
    Error("GetBinError","Unrecognised sample: %s", sampleName);
    return 0;
  }
  double out = 0;
  
  //will scale result by the coefficient
  RooAbsArg* coefArg = (sampleNumber>=0) ? chan->coeffList().at(sampleNumber) : 0;

  if(samp->InheritsFrom("TRooAbsH1")) {
    //the simple case :-)
    out = (dynamic_cast<TRooAbsH1*>(samp))->GetBinError(bin,fr);
    //scale by coef ...
    double coef = (coefArg) ? static_cast<RooAbsReal*>(coefArg)->getVal() : 1.;
    out *= coef;
    return out;
  }
  
  Error("GetBinError","Not supported for this individual sample");
  return 0;
  
}

double TRooWorkspace::GetBinContent(const char* sampleName, const char* channelName, Int_t bin, const TRooFitResult& fr) {
  auto chan = channel(channelName);
  if(!chan) {
    Error("GetBinContent","Unrecognised channel: %s",channelName);
    return 0;
  }

  int sampleNumber = -1;
  RooAbsReal* samp = 0;
  if(sampleName==0 || strlen(sampleName)==0 || data(sampleName)) {
      // getting a data yield ...
      RooAbsData* _data = data((sampleName==0 || strlen(sampleName)==0) ? fCurrentData.Data() : sampleName);
      if(!_data) return 0;
      //get observable for channel ...
      RooAbsReal* chanFunc = function(channelName);
      if(!chanFunc) return 0;
      RooArgSet* chanObs = chanFunc->getObservables(set("obs"));
      if(chanObs->getSize()!=1) {
          Error("GetBinContent","More than one observable found?"); //FIXME: support for 2D would need this fixing
          return 0;
      }
      RooAbsArg* arg = chanObs->first();
      int abin[3];
      fDummyHists[channelName]->GetBinXYZ(bin,abin[0],abin[1],abin[2]);
      double low[3];
      low[0] = fDummyHists[channelName]->GetXaxis()->GetBinLowEdge(abin[0]);
      double high[3];
      high[0] = fDummyHists[channelName]->GetXaxis()->GetBinUpEdge(abin[0]);
      double yield = _data->sumEntries(Form("%s==%s::%s&&(%s>=%f)&&(%s<%f)",fChannelCatName.Data(),fChannelCatName.Data(),channelName,arg->GetName(),low[0],arg->GetName(),high[0]));
      delete chanObs;
      return yield;

  } else if(strcmp(sampleName,channelName)==0) {
      samp = dynamic_cast<RooAbsReal *>(chan);
  } else {
    sampleNumber = chan->compList().index(Form("%s_%s",sampleName,channelName));
    if(sampleNumber==-1) sampleNumber = chan->compList().index(sampleName);
    if(sampleNumber==-1) return 0;
    samp = dynamic_cast<RooAbsReal*>(chan->compList().at(sampleNumber));
  }
  
  if(!samp) {
    Error("GetBinContent","Unrecognised sample: %s", sampleName);
    return 0;
  }
  double out = 0;
  
  //will scale result by the coefficient
  RooAbsArg* coefArg = (sampleNumber>=0) ? chan->coeffList().at(sampleNumber) : 0;

  if(samp->InheritsFrom("TRooAbsH1")) {
    //the simple case :-)
    out = (dynamic_cast<TRooAbsH1*>(samp))->GetBinContent(bin,fr);
    //scale by coef ...
    double coef = (coefArg) ? static_cast<RooAbsReal*>(coefArg)->getVal() : 1.;
    out *= coef;
    return out;
  }

  //got here, so apply the fr if necessary, then evaluate subcomponent
  //Warning: TRooAbsH1's GetBinContent doesn't perform an integral, only an evaluation at the bin center
  //but here we use a runningIntegral object, which gets the correct value ..

  if(bin<=0) return 0;

  RooArgSet* myObs = samp->getObservables(*const_cast<TRooWorkspace*>(this)->set("obs"));
  if(myObs->getSize()>1) {
    Error("GetBinContent","2D not supported yet");
    delete myObs;
    return 0;
  }
  
  
  
  //std::unique_ptr<RooAbsReal> inte( ((RooAbsPdf*)samp)->createCdf(*myObs) );
  
  RooProduct prod("prod","prod",(coefArg) ? RooArgList((*samp),*coefArg) : RooArgList((*samp))); //sampe was inte when I was using integral
  
  RooArgSet* params = 0;
  RooArgSet* snap = 0;

  
  if(fr.floatParsFinal().getSize()||fr.constPars().getSize()) {
      params = samp->getParameters(*myObs);
      snap = (RooArgSet*)params->snapshot();
      *params = fr.floatParsFinal();
      *params = fr.constPars();
  }

  RooAbsRealLValue* obs = dynamic_cast<RooAbsRealLValue*>(myObs->first());
  double tmpVal = obs->getVal();
  
  /* FIXME : This integration isn't working???
  obs->setVal( obs->getBinning(channelName).binLow(bin-1) - 1e-12 ); prod.setValueDirty();
  if( fabs(obs->getMin()-obs->getVal()) > 1e-9 ) {
    std::cout << inte->getVal() << std::endl;
    obs->Print();
    out -= prod->getVal(); //don't need to subtract if already at minimum value
  }
  obs->setVal( obs->getBinning(channelName).binHigh(bin-1)+1e-12 );
  obs->Print();
  out += prod->getVal();
  std::cout << inte->getVal() << std::endl;
  */
  
  
  obs->setBin( bin - 1, channelName );
  
  out += prod.getVal(myObs)*obs->getBinning(channelName).binWidth(bin-1);//*( (RooAbsPdf*)samp)->expectedEvents(*myObs);
  
  obs->setVal(tmpVal);

  if(fr.floatParsFinal().getSize()||fr.constPars().getSize()) {
    *params = *snap;
    delete snap;
  }
  delete myObs;
  
  

  return out;
  
}

double TRooWorkspace::GetBinContent(const char* channelName, Int_t bin) {
    return GetBinContent(channelName,channelName,bin,fCurrentFitResult?*fCurrentFitResult:"");
}

double TRooWorkspace::GetSampleCoefficient(const char* sampleFullName) const {
  RooAbsReal* samp = function(sampleFullName);
  if(!samp) {
    Error("GetSampleCoefficient","Unknown sample: %s",sampleFullName);
    return 1.;
  }
  //need to determine which channel this sample belongs to, because coefficient on sample may not be 1 (this is case in histfactory models) 
  std::unique_ptr<TIterator> catIter(cat(fChannelCatName)->typeIterator());
  TObject* c;
  while( (c = catIter->Next()) ) {
    if(function(c->GetName())->dependsOn(*samp)) break;
  }
  int idx = channel(c->GetName())->compList().index(samp);
  if(idx!=-1) {
    return dynamic_cast<RooAbsReal*>(channel(c->GetName())->coeffList().at(idx))->getVal();
  } 
  Error("GetSampleCoefficient","Failed to find sample coefficient!");
  return 1;
}



double TRooWorkspace::IntegralAndError(double& err, const char* sampleName, const char* channelName, const TRooFitResult& fr, const char* errGroup) const {
  
  int sampleNumber=-1;
  RooAbsReal* samp = 0;
  
  if(sampleName==0) {
    //want a channel integral 
    samp = dynamic_cast<RooAbsReal*>(channel(channelName));
  } else {

    sampleNumber = channel(channelName)->compList().index(Form("%s_%s",sampleName,channelName));
    if(sampleNumber==-1) sampleNumber = channel(channelName)->compList().index(sampleName);
    if(sampleNumber==-1) return 0;
  
    samp = dynamic_cast<RooAbsReal*>(channel(channelName)->compList().at(sampleNumber));
  }
  if(!samp) return 0;
  double out = 0;
  
  //first check if the coefficient is simple or derived
  RooAbsArg* coefArg = (sampleNumber!=-1) ? channel(channelName)->coeffList().at(sampleNumber) : 0;
  if((coefArg && coefArg->isDerived()) || !samp->InheritsFrom("TRooAbsH1")) {
    //arg may depend on parameters in e.g. the fit result, so we will combine integral with coeffient in a RooProduct before getting the error ...
    
    RooArgSet* myObs = samp->getObservables(*const_cast<TRooWorkspace*>(this)->set("obs"));
    
    //if sample is in a RooAddPdf, then it should be auto-normalized, (FIXME: unless allExtendable is true), so only thing that impacts the result is the coefficent
    std::unique_ptr<RooAbsReal> inte( (function(channelName)->InheritsFrom(RooAddPdf::Class())) ? new RooRealVar("const1","const",1) : samp->createIntegral(*myObs) );
    
    RooProduct prod("prod","prod",(coefArg) ? RooArgList((*inte),*coefArg) : RooArgList(*inte));
    
    RooArgSet* params = 0;
    RooArgSet* snap = 0;
  
    if(fr.floatParsFinal().getSize()||fr.constPars().getSize()) {
        params = samp->getParameters(*myObs);
        snap = (RooArgSet*)params->snapshot();
        *params = fr.floatParsFinal();
        *params = fr.constPars();
    }
  
    out = prod.getVal();
    auto res = TRooAbsH1::getError(fr,&prod,*myObs,0,errGroup);
    
    err = res.first;
    
    if(channel(channelName)->coeffList().getSize()==0 && samp->InheritsFrom("RooAbsPdf")) {
      //assume we are in a RooAddPdf where none of the coefficients are provided, so all the terms need scaling by their integrals 
      double e = dynamic_cast<RooAbsPdf*>(samp)->expectedEvents(*myObs);
      out *= e; err *= e;
      
    }
  
    if(fr.floatParsFinal().getSize()||fr.constPars().getSize()) {
      *params = *snap;
      delete snap;
    }
    delete myObs;
    
  } else if(samp->InheritsFrom("TRooAbsH1")) {
    out = (dynamic_cast<TRooAbsH1*>(samp))->IntegralAndError(err,fr,errGroup);
    //scale by coef ...
    double coef = (coefArg) ? static_cast<RooAbsReal*>(coefArg)->getVal() : 1.;
    err *= coef;
    out *= coef;
  } 
  return out;
}

// double TRooWorkspace::sampleIntegralAndError(double& err, const char* sampleFullName, const TRooFitResult& fr) const {
//   RooAbsReal* samp = function(sampleFullName); //retrieve pdf too, if exists
//   if(!samp) return 0;
//   double out = 0;
//   if(samp->InheritsFrom("TRooAbsH1")) {
//     out = (dynamic_cast<TRooAbsH1*>(samp))->IntegralAndError(err,fr);;
//   } else {
//     //just a normal function, so we have to manually do integral and error ...
//     RooArgSet* myObs = samp->getObservables(*const_cast<TRooWorkspace*>(this)->set("obs"));
//    std::unique_ptr<RooAbsReal> inte( samp->createIntegral(*myObs) );
//   
//     RooArgSet* params = 0;
//     RooArgSet* snap = 0;
//   
//     if(fr.floatParsFinal().getSize()||fr.constPars().getSize()) {
//         params = samp->getParameters(*myObs);
//         snap = (RooArgSet*)params->snapshot();
//         *params = fr.floatParsFinal();
//         *params = fr.constPars();
//     }
//   
//     out = inte->getVal();
//     auto res = TRooAbsH1::getError(fr,&*inte,*myObs,0);
//     
//     err = res.first;
//   
//     if(fr.floatParsFinal().getSize()||fr.constPars().getSize()) {
//       *params = *snap;
//       delete snap;
//     }
//     delete myObs;
//   }
//   double coef = GetSampleCoefficient(sampleFullName);
//   err *= coef;
//   out *= coef;
//   
//   return out;
//   
// }

TRooAbsH1Fillable* TRooWorkspace::sample(const char* sampleName, const char* channelName) {
  if(!channelName || strlen(channelName)==0){
      return dynamic_cast<TRooAbsH1Fillable*>(function(sampleName));
  }
  RooAbsReal* chan = dynamic_cast<RooAbsReal*>(channel(channelName));
  TRooH1* out = dynamic_cast<TRooH1*>(chan->findServer(Form("%s_%s",sampleName,channelName)));
  return out;
}

TRooAbsHStack* TRooWorkspace::channel(const char* name) const {
  TRooAbsHStack* out =  dynamic_cast<TRooAbsHStack*>(function(name));
  /*if(!out) {
    out = dynamic_cast<TRooAbsHStack*>(fStagedChannels.find(name));
  }*/
  return out;
}
  

//set fill color of samples in given channels
void TRooWorkspace::SetFillColor(const char* sampleName,const char* channelNames, Int_t in) {
  std::vector<TRegexp> patterns;
  TStringToken pattern(channelNames,";");
  while(pattern.NextToken()) {
    patterns.emplace_back( TRegexp(pattern,true) );
  }
  
  
  //loop over channels, create sample for each one that matches pattern
  std::unique_ptr<TIterator> catIter(cat(fChannelCatName)->typeIterator());
  TObject* c;
  while( (c = catIter->Next()) ) {
    TString cName(c->GetName());
    bool matched(false);
    for(auto& p : patterns) if(cName.Contains(p)) { matched=true; break; }
    if(!matched) continue;
    if(sample(sampleName,c->GetName())) sample(sampleName,c->GetName())->SetFillColor(in);
  }
}
void TRooWorkspace::SetLineColor(const char* sampleName,const char* channelNames, Int_t in) {
  std::vector<TRegexp> patterns;
  TStringToken pattern(channelNames,";");
  while(pattern.NextToken()) {
    patterns.emplace_back( TRegexp(pattern,true) );
  }
  
  
  //loop over channels, create sample for each one that matches pattern
  std::unique_ptr<TIterator> catIter(cat(fChannelCatName)->typeIterator());
  TObject* c;
  while( (c = catIter->Next()) ) {
    TString cName(c->GetName());
    bool matched(false);
    for(auto& p : patterns) if(cName.Contains(p)) { matched=true; break; }
    if(!matched) continue;
    if(sampleName==0) channel(c->GetName())->SetLineColor(in);
    else if(sample(sampleName,c->GetName())) sample(sampleName,c->GetName())->SetLineColor(in);
  }
  
}

void TRooWorkspace::SetLineWidth(const char* sampleName,const char* channelNames, Int_t in) {
  std::vector<TRegexp> patterns;
  TStringToken pattern(channelNames,";");
  while(pattern.NextToken()) {
    patterns.emplace_back( TRegexp(pattern,true) );
  }
  
  
  //loop over channels, create sample for each one that matches pattern
  std::unique_ptr<TIterator> catIter(cat(fChannelCatName)->typeIterator());
  TObject* c;
  while( (c = catIter->Next()) ) {
    TString cName(c->GetName());
    bool matched(false);
    for(auto& p : patterns) if(cName.Contains(p)) { matched=true; break; }
    if(!matched) continue;
    if(sampleName==0) channel(c->GetName())->SetLineWidth(in);
    else if(sample(sampleName,c->GetName())) sample(sampleName,c->GetName())->SetLineWidth(in);
  }
  
}

void TRooWorkspace::Scale(const char* sampleName,const char* channelNames,RooAbsReal& arg) {
  std::vector<TRegexp> patterns;
  TStringToken pattern(channelNames,";");
  while(pattern.NextToken()) {
    patterns.emplace_back( TRegexp(pattern,true) );
  }
  
  
  //loop over channels, create sample for each one that matches pattern
  std::unique_ptr<TIterator> catIter(cat(fChannelCatName)->typeIterator());
  TObject* c;
  while( (c = catIter->Next()) ) {
    TString cName(c->GetName());
    bool matched(false);
    for(auto& p : patterns) if(cName.Contains(p)) { matched=true; break; }
    if(!matched) continue;
    auto samp = sample(sampleName,c->GetName());
    if(samp) {
        // check if the arg we are able to scale by depends on observables the sample doesnt yet
        // depend on ... if we scale by this arg it will turn the sample into a joint distribution
        // of the observables
        auto obs = set("obs");
        if (obs) {
            std::unique_ptr<TIterator> itr(obs->createIterator());
            RooAbsArg* a = 0;
            while((a=(RooAbsArg*)itr->Next())) {
                if(arg.dependsOn(*a) && !(((RooAbsReal*)samp)->dependsOn(*a))) {
                    Warning("Scale","Scaling %s by %s is joining %s observable to distribution",
                            samp->GetName(),arg.GetName(),a->GetName());
                }
            }
        }
        samp->Scale(arg);
        // check if the arg we just scaled by depends on any observables that
    }
  }
}




RooSimultaneous* TRooWorkspace::model(const char* channels) {
  //builds the model for the given channels, putting them in a RooSimultaneous, then imports that and returns
  
  std::vector<TRegexp> patterns;
  TStringToken nameToken(channels,";");
  while(nameToken.NextToken()) {
      TString subName = (TString)nameToken;
      patterns.push_back(TRegexp(subName,true));
  }
  
  TRegexp pattern(channels,true);
  std::unique_ptr<TIterator> catIter(cat(fChannelCatName)->typeIterator());
  TObject* c;
  
  TString simPdfName(fSimPdfName);
  int nComps(0);
  TString factoryString("");
  
  RooSimultaneous* simPdf = dynamic_cast<RooSimultaneous*>(pdf(simPdfName));
  
  while( (c = catIter->Next()) ) {
    bool pass=false;
    for(auto& pattern : patterns) if(TString(c->GetName()).Contains(pattern)) pass=true;
    if(pass==false) continue;
    if(function(c->GetName())->getAttribute("isValidation")) continue; //don't include validation regions when constructing models
    nComps++;
    // externalConstraints at the channel level are added into the RooProdPdf of the channel
    const RooArgSet* externalConstraints = 0;
    if (function(c->GetName())->getStringAttribute("externalConstraints")) {
      externalConstraints = set(function(c->GetName())->getStringAttribute("externalConstraints"));
    }
    if(simPdf) {
      //take the channel models from the existing simPdf, to handle case where looking at histfactory model
      if(!simPdf->getPdf(c->GetName())) {
        //user added another channel ...
        Info("model","Adding channel %s",c->GetName());
        if( !pdf(Form("%s_with_Constraints",c->GetName())) ) {
          import( *channel(c->GetName())->buildConstraints(*set("obs"),"",true,externalConstraints), RooFit::RecycleConflictNodes(), RooFit::Silence() );
        }
        simPdf->addPdf(*pdf(Form("%s_with_Constraints",c->GetName())),c->GetName());
      }
      factoryString += Form(",%s=%s",c->GetName(),simPdf->getPdf(c->GetName())->GetName());
    } else {
      if( !pdf(Form("%s_with_Constraints",c->GetName())) ) {
        import( *channel(c->GetName())->buildConstraints(*set("obs"),"",true,externalConstraints), RooFit::RecycleConflictNodes(), RooFit::Silence() );
      }
      factoryString += Form(",%s=%s_with_Constraints",c->GetName(),c->GetName());
      //remove from the stagedChannels list if its there (FIXME: is this a memory leak?)
      //fStagedChannels.remove(*channel(c->GetName()),true/*silent*/,true/*match by name*/);
    }
    simPdfName += Form("_%s",c->GetName());
  }
  
  if(nComps>0) {
    if(nComps==cat(fChannelCatName)->numTypes()) { simPdfName=fSimPdfName; } //all channels available
    if(pdf(simPdfName)) return static_cast<RooSimultaneous*>(pdf(simPdfName));
    factory(Form("SIMUL::%s(%s%s)",simPdfName.Data(),fChannelCatName.Data(),factoryString.Data()));
    
    // ensure any cached sets in the ws are removed
      std::vector<std::string> removeSets;
      for (std::map<std::string,RooArgSet>::const_iterator it = _namedSets.begin() ; it !=
      _namedSets.end() ; ++it) {
          if (TString(it->first.c_str()).BeginsWith("CACHE_")) {
              removeSets.emplace_back(it->first);
          }
      }
      if (!removeSets.empty()) {
          Info("model","Removing %lu cached sets from workspace",removeSets.size());
          for (auto &s : removeSets) removeSet(s.c_str());
      }
    
    //if got here then need to create the global observables snapshot too ...
    //need list of gobs then ..
    RooAbsPdf* m = pdf(simPdfName);
     //infer the global observables, nuisance parameters, model args (const np) 
    RooArgSet* gobs_and_np = m->getParameters(*set("obs"));

    //remove the poi ...
    //if(set("poi")) gobs_and_np->remove(*set("poi"));

    RooArgSet _gobs;
    _gobs.add(*gobs_and_np); //will remove the np in a moment

    //now pass this to the getAllConstraints method ... it will modify it to contain only the np!
    RooArgSet* s = m->getAllConstraints(*set("obs"),*gobs_and_np);
     delete s; //don't ever need this 

     //gobs_and_np now only contains the np
     _gobs.remove(*gobs_and_np);
     //ensure all global observables are held constant now - important for pll evaluation
     _gobs.setAttribAll("Constant",kTRUE);
     
     defineSet(Form("globalObservables%s",TString(simPdfName(fSimPdfName.Length(),simPdfName.Length()-fSimPdfName.Length())).Data()),_gobs);
     
     saveSnapshot(Form("%s_globalObservables%s",fCurrentData.Data(),TString(simPdfName(fSimPdfName.Length(),simPdfName.Length()-fSimPdfName.Length())).Data()),_gobs);
     
     //save the list of parameters as a set (poi+np)
     RooArgSet np;RooArgSet args;
      std::unique_ptr<TIterator> itr(gobs_and_np->createIterator());
      RooAbsArg* arg = 0;
      while((arg=(RooAbsArg*)itr->Next())) { 
          if(arg->isConstant()) args.add(*arg);
          else np.add(*arg);
      }
      defineSet(Form("params%s",TString(simPdfName(6,simPdfName.Length()-6)).Data()),np);
      defineSet(Form("args%s",TString(simPdfName(6,simPdfName.Length()-6)).Data()),args);
     
     delete gobs_and_np;
     
  }
  
  return static_cast<RooSimultaneous*>(pdf(simPdfName));
}

bool TRooWorkspace::generateAsimov(const char* name, const char* title, bool fitToObsData) {
  RooAbsPdf* thePdf = model();

  RooArgSet* snap = 0;

  if(fitToObsData && data(fCurrentData)) {
    //fit to data first, holding parameters of interest constant at their current values ...
    snap = (RooArgSet*)allVars().snapshot();
    if(set("poi")) {
      const_cast<RooArgSet*>(set("poi"))->setAttribAll("Constant",kTRUE);
    }
    
    RooFitResult* fitResult = fitTo(data(),data_gobs(),"qn" /*no need for hesse*/); //fits to the "current data" but ensures it doesn't "save" nll to workspace (because it's conditional)
    
    //import(*fitResult,Form("conditionalFit_%s_for%s",fCurrentData.Data(),name));
    delete fitResult;
  }
  
  auto asimov = TRooFit::generateAsimovDataset(thePdf,set("obs"),gobs());
  
  
  asimov.first->SetName(name);asimov.first->SetTitle(title);
  
  import(asimov);
  
  delete asimov.first;
  delete asimov.second;
  
  if(snap) {
    allVars() = *snap;
    delete snap;
  }
  
  return kTRUE;
  
}

std::pair<RooAbsData*,RooArgSet*> TRooWorkspace::generateToy(const char* name, const char* title, bool fitToObsData, RooAbsPdf::GenSpec** gs) {
    TString sName(name); TString sTitle(title);
  RooAbsPdf* thePdf = model();

  RooArgSet* snap = 0;

  if(fitToObsData && data(fCurrentData)) {
    //fit to data first, holding parameters of interest constant at their current values ...
    snap = (RooArgSet*)set("poi")->snapshot();
    if(set("poi")) const_cast<RooArgSet*>(set("poi"))->setAttribAll("Constant",kTRUE);
    RooFitResult* fitResult = fitTo(data(),data_gobs(),"qn" /*no need for hesse*/); //fits to the "current data"
    //import(*fitResult,Form("conditionalFit_%s_for%s",fCurrentData.Data(),name));
    delete fitResult;
  }
  
  auto out = TRooFit::generateToy(thePdf,set("obs"),gobs(),false/*gen binned*/,gs);
  
  if(out.second) out.second->setName(Form("%s_globalObservables",sName.Data()));
  out.first->SetName(sName);out.first->SetTitle(sTitle);
  
  if(snap) {
    allVars() = *snap;
    delete snap;
  }
  
  return out;
  
}

TGraph* TRooWorkspace::DrawPLL(const char* parName, const char* opt, int nPoints) {
  
  RooAbsReal* _nll = getFitNll();
  if(!_nll) return 0;
  
  RooFitResult* _fit = getFit();
  
  RooRealVar* _var = var(parName);
  
  
  
  TString sOpt(opt);
  
  TVirtualPad* pad = gPad;
  if(!pad) {
    gROOT->MakeDefCanvas();
    pad = gPad;
  }
  
  TGraph* g = new TGraph;
  g->GetHistogram()->GetXaxis()->SetTitle(_var->GetTitle()); 
  g->GetHistogram()->GetYaxis()->SetTitle("PLL");
  g->SetName(Form("pll_%s",parName)); g->SetTitle(Form("PLL(%s)",_var->GetTitle()));
  g->SetLineColor(kRed); g->SetLineWidth(2);
  if(sOpt.Contains("nll")) {
    g->SetLineColor(kBlue);
    sOpt.ReplaceAll("nll","");
    g->GetHistogram()->GetYaxis()->SetTitle("#Delta NLL");
  }
  g->SetBit(kCanDelete);
  g->Draw(sOpt);
  
  double minNll = _fit->minNll();
  
  RooArgSet* snap = (RooArgSet*)allVars().snapshot();
  
  //the order of evaluation will start at the minima and work out ...
  double val = ((RooRealVar*)_fit->floatParsFinal().find(*_var))->getVal();
  double xStep2 = (_var->hasError()) ? _var->getError()*(4./(nPoints)) : ((_var->getMax()-_var->getMin())/nPoints); //how much we will step by 
  double xStep = xStep2; //the next step 
  
  //first point will be the minima ...
  g->SetPoint( g->GetN(), val, 0 );
  
  
  for(int i=0;i<(nPoints);i++) {
    if((i%2)==0) val += xStep;
    else val -= xStep;
    xStep += xStep2;
  
  
    if(val > _var->getMax()) continue;
    if(val < _var->getMin()) continue;
  
    //double val = _var->getMin() + i*(_var->getMax()-_var->getMin())/20.;
    _var->setConstant();
    _var->setVal(val);
    
    if(g->GetLineColor()==kRed) {
      RooFitResult* result = TRooFit::minimize(_nll, true, false); //no hesse
      if(result->status() && result->floatParsFinal().getSize()) {
        delete result;
        allVars() = *snap; //resets
        continue;
      }
      delete result;
    }
    
    g->SetPoint( g->GetN() , val, 2.*(_nll->getVal() - minNll) );
    g->Sort(); //sorts the points in the x-axis direction 
    
    allVars() = *snap;
    pad->Modified(1);
    pad->Update();
   }
   
   delete snap;
    
    
    
   return g;
  

}

double TRooWorkspace::pll(const char* _poi, RooAbsData* theData, const RooArgSet* globalObservables, bool oneSided, bool discovery) {
  return pll(*var(_poi),theData,globalObservables,oneSided,discovery);
}

double TRooWorkspace::pll(RooArgSet&& _poi, RooAbsData* theData, const RooArgSet* globalObservables, bool oneSided, bool discovery) {
  if(oneSided && discovery) {
    return pll(std::move(_poi),theData,globalObservables,TRooFit::OneSidedNegative);
  } else if(oneSided && !discovery) {
    return pll(std::move(_poi),theData,globalObservables,TRooFit::OneSidedPositive);
  } else if(!oneSided) {
    return pll(std::move(_poi),theData,globalObservables,TRooFit::TwoSided);
  }
  return std::numeric_limits<double>::quiet_NaN();
}

double TRooWorkspace::pll(RooArgSet&& _poi, RooAbsData* theData, const RooArgSet* globalObservables, bool (*_compatibilityFunction)(double mu, double mu_hat), RooAbsReal** _nll, RooFitResult** _unconditionalFit) {
  
  
  //do an unconditional fit ...
  RooArgSet* snap = (RooArgSet*)allVars().snapshot();
  TRooFit::setAttribAll(_poi,"Constant",kFALSE);
  
  //if(_unconditionalFit && *_unconditionalFit) std::cout << "reusing unconditional" << std::endl;
  
  RooFitResult* unconditionalFit = 0;
  
  if(_unconditionalFit && *_unconditionalFit) {
    unconditionalFit = *_unconditionalFit;
    //put float parameters to unconditionalFit values ...
    allVars() = unconditionalFit->floatParsFinal();
  } else {
    //n option means don't have any nll that is created automatically (if required) in the fNll list
    unconditionalFit = fitTo(theData,globalObservables,"qn" /*no need for hesse*/,_nll);
  }
  
  //check if fit was bad ... if so, return NaN
  if((unconditionalFit->status()%10)!=0 || std::isnan(unconditionalFit->minNll())) {
    Error("pll","Bad Unconditional Fit Result is status %d ... returning NaN",unconditionalFit->status());
    if(!_unconditionalFit) delete unconditionalFit;
    else *_unconditionalFit = unconditionalFit;
    allVars() = *snap; //restores values
    delete snap;
    return std::numeric_limits<double>::quiet_NaN();
  }
  
  //put the just the poi back to their values (in prep for the conditional fit) ...
  _poi = *snap;
  
  double sf = 1.;
  
  if(_compatibilityFunction(  ((RooAbsReal*)_poi.first())->getVal() , ((RooAbsReal*)unconditionalFit->floatParsFinal().find(_poi.first()->GetName()))->getVal() ) == 1) {
    if(_compatibilityFunction==TRooFit::Uncapped) {
      //when doing the uncapped test statistic, we will flip the pll value when mu_hat is "compatible" with mu
      sf = -1.;
    } else {
      //best fit poi value is considered compatible with test value 
      if(!_unconditionalFit) delete unconditionalFit;
      else *_unconditionalFit = unconditionalFit;
      allVars() = *snap; //restores values
      delete snap;
      return 0;
    }
  }
  
  
  //do a conditional fit ...
  TRooFit::setAttribAll(_poi,"Constant",kTRUE);
  RooFitResult* conditionalFit = fitTo(theData,globalObservables,"qn" /*no need for hesse*/,_nll);
  
  //check if fit was bad ... if so, return NaN
  if((conditionalFit->floatParsFinal().getSize() && (conditionalFit->status()%10)!=0) || std::isnan(conditionalFit->minNll())) {
    Error("pll","Bad Conditional Fit Result is status %d ... returning NaN (poi: %s)",conditionalFit->status(),_poi.contentsString().c_str());
    if(!_unconditionalFit) delete unconditionalFit;
    else *_unconditionalFit = unconditionalFit;
    allVars() = *snap; //restores values
    delete snap;
    return std::numeric_limits<double>::quiet_NaN();
  }
  
  //check if conditionalFit beat unconditional (shouldn't happen)
  if( (conditionalFit->minNll() - unconditionalFit->minNll()) < -0.00001) {
      auto old_ucNll = unconditionalFit->minNll();
      auto old_muHat = dynamic_cast<RooRealVar*>(unconditionalFit->floatParsFinal().find(_poi.first()->GetName()))->getVal();
    delete unconditionalFit;
    TRooFit::setAttribAll(_poi,"Constant",kFALSE);
    unconditionalFit = fitTo(theData,globalObservables,"qn" /*no need for hesse*/,_nll);
    if((conditionalFit->minNll() - unconditionalFit->minNll()) < -0.00001) {
      Error("pll","Conditional Fit NLL (%f) smaller than Unconditional Fit NLL (%f) even after refit ... returning NaN (poi: %s)",conditionalFit->minNll(),unconditionalFit->minNll(),_poi.contentsString().c_str());
      if(!_unconditionalFit) delete unconditionalFit;
      else *_unconditionalFit = unconditionalFit;
      delete conditionalFit;
      allVars() = *snap; //restores values
      delete snap;
      return std::numeric_limits<double>::quiet_NaN();
      //throw std::runtime_error("Conditional Fit better than Unconditional Fit");
    } else {
        Warning("pll","Conditional Fit NLL (%f) smaller than Unconditional Fit NLL (%f, %s=%f) ... reran = %f, %s=%f",conditionalFit->minNll(),old_ucNll,_poi.first()->GetName(),old_muHat,unconditionalFit->minNll(),_poi.first()->GetName(),dynamic_cast<RooRealVar*>(unconditionalFit->floatParsFinal().find(_poi.first()->GetName()))->getVal());
    }
  }
  
  allVars() = *snap; //restores values
  
  double out = sf*2.0*(conditionalFit->minNll() - unconditionalFit->minNll());
  
  
  if(!_unconditionalFit) delete unconditionalFit;
  else *_unconditionalFit = unconditionalFit;
  delete conditionalFit;
  delete snap;
  
  return out;
  
  
}

const RooArgSet* TRooWorkspace::data_gobs(const char* dataName) const {
  //get the current model name ...
  std::unique_ptr<TIterator> catIter(cat(fChannelCatName)->typeIterator());
  TObject* c;
  TString simPdfName(fSimPdfName);
  int nComps(0);
  while( (c = catIter->Next()) ) {
    if(function(c->GetName())->getAttribute("isValidation")) continue; //don't include validation regions when constructing models
    nComps++;
    simPdfName += Form("_%s",c->GetName());
  }
  if(nComps==cat(fChannelCatName)->numTypes()) { simPdfName=fSimPdfName; } //all channels available
  
  TString theData = (dataName==0) ? fCurrentData.Data() : dataName;
  
  return getSnapshot(Form("%s_globalObservables%s",theData.Data(),TString(simPdfName(fSimPdfName.Length(),simPdfName.Length()-fSimPdfName.Length())).Data()));
}

const RooArgSet* TRooWorkspace::gobs() {
  //get the current model name ...
  std::unique_ptr<TIterator> catIter(cat(fChannelCatName)->typeIterator());
  TObject* c;
  TString simPdfName(fSimPdfName);
  int nComps(0);
  while( (c = catIter->Next()) ) {
    if(function(c->GetName())->getAttribute("isValidation")) continue; //don't include validation regions when constructing models
    nComps++;
    simPdfName += Form("_%s",c->GetName());
  }
  if(nComps==cat(fChannelCatName)->numTypes()) { simPdfName=fSimPdfName; } //all channels available
  
  return set(Form("globalObservables%s",TString(simPdfName(fSimPdfName.Length(),simPdfName.Length()-fSimPdfName.Length())).Data()));
  
}


#include "TMultiGraph.h"

template <class T> struct greater_abs {
        bool operator() (const T& x, const T& y) const {return fabs(x)>fabs(y);}
      };


RooFitResult* TRooWorkspace::fitTo(RooAbsData* theData, const RooArgSet* globalObservables, Option_t* opt, RooAbsReal** _nllToReuse) {
  TString sOpt(opt);sOpt.ToLower();
  bool doHesse = sOpt.Contains("h");
  bool internalData = (data(theData->GetName())==theData); //data is already saved into workspace?
  TString fitTitle = TString::Format("Fit to %s",theData->GetTitle());
  RooAbsReal* _nll = 0;
  
  RooAbsData* reducedData=0;
  
  if(_nllToReuse) _nll = *_nllToReuse;
  
  //only do reduction tests when not reusing an nll -- worried about overhead here
  if(_nll==0) {
  
    RooSimultaneous* thePdf = model();
    TString simPdfName(thePdf->GetName());
    
    
    
    //check if we need to reduce the data (remove validation regions)
    std::unique_ptr<TIterator> catIter(cat(fChannelCatName)->typeIterator());
    TObject* c;
    TString excludedChannels;
    std::set<TString> excludedChannelsSet;
    while( (c = catIter->Next()) ) {
      if(!thePdf->getPdf(c->GetName())) {
        excludedChannelsSet.insert(c->GetName());
        if(excludedChannels!="") excludedChannels += " || ";
        excludedChannels += Form("%s == %d",fChannelCatName.Data(),((RooCatType*)c)->getVal());
      }
    }
    
    
    
    
    
    
    if(excludedChannels!="" && theData->sumEntries(excludedChannels)>0) {
      fitTitle = TString::Format("Restricted %s (%d channels)",fitTitle.Data(),int(cat(fChannelCatName)->numTypes() - excludedChannelsSet.size()));
      TString dataName = TString::Format("reduced_%s_for_%s",theData->GetName(),thePdf->GetName());
      if(!internalData || !embeddedData(dataName)) {
        if(!sOpt.Contains("q")) Info("fitTo","reducing %s to exclude validation channels",theData->GetName());
        TString cutString="!(";cutString+=excludedChannels;cutString+=")";
        
        //discovered that asimov datasets aren't properly reduced, they have been losing their weights ... so will do a manual reduction ..
        //if dataset is weighted
        if(theData->isWeighted()) {
          RooArgSet obs(*theData->get());RooRealVar weightVar("weightVar","weightVar",1);
          obs.add(weightVar);
          reducedData = new RooDataSet(theData->GetName(),theData->GetTitle(),obs,"weightVar");
          for(int i=0;i<theData->numEntries();i++) {
            if(excludedChannelsSet.find(theData->get(i)->getCatLabel(fChannelCatName))!=excludedChannelsSet.end()) continue; //skip excluded channels
            
            reducedData->add(*theData->get(),theData->weight());
          }
        } else {
          reducedData = theData->reduce(cutString);
        }
        
        reducedData->SetName(dataName);
        if(internalData) {
          import(*reducedData,RooFit::Embedded());
          delete reducedData;
        }
      }
      theData = (internalData) ? embeddedData(dataName) : reducedData;
    }
  
    _nll = (sOpt.Contains("r") || !internalData) ? 0 : nll(Form("nll_%s_%s",theData->GetName(),thePdf->GetName()));
    
    if(!_nll) {
      //it seems that if any parameters are const and we later make them unconst then the nll doesnt update properly (the const optimization is borked) 
      //so unconst all poi to be safe ...
      RooArgSet* snap = 0;
      if(set("poi")) {
        snap = (RooArgSet*)set("poi")->snapshot();
        TRooFit::setAttribAll(*const_cast<RooArgSet*>(set("poi")),"Constant",kFALSE);
      }
      const RooArgSet* _gobs = gobs();

      // externalConstraints of the whole model are attached at the NLL object level
      const RooArgSet* externalConstraints = 0;
      if (thePdf->getStringAttribute("externalConstraints")) {
         externalConstraints = set(thePdf->getStringAttribute("externalConstraints"));
         Info("fitTo","Building NLL with external constraints %s",thePdf->getStringAttribute("externalConstraints"));
      }
      _nll = TRooFit::createNLL(thePdf,theData,_gobs,externalConstraints);
      if(snap) {
        *const_cast<RooArgSet*>(set("poi")) = *snap;
        delete snap;
      }
      _nll->SetName(Form("nll_%s_%s",theData->GetName(),thePdf->GetName()));

      if(internalData && !sOpt.Contains("n")) {
        //import(*_nll/*,RooFit::Silence()*/);
        //delete _nll;
        //Discovered that imported nll does not behave same as unimported version (fits not converging)
        //next couple of lines were attempt to resolve difference but not successful
        //hence now TRooWorkspace tracks its own nll in a list ...
        //RooAbsReal* constr = function(Form("nll_%s_%s_constr",thePdf->GetName(),theData->GetName()));
        //if(constr) constr->setOperMode(RooAbsArg::ADirty);
        //_nll = function(Form("nll_%s_%s",theData->GetName(),thePdf->GetName()));
        
        //check for existing nll (will happen when we are 'resetting' the fit
        RooAbsArg* oldNll = fNll.find(_nll->GetName());
        if(oldNll) {
            fNll.remove(*oldNll); // not removing by match name only ... so must manually delete even though is owner
            delete oldNll;
        }

        fNll.addOwned(*_nll);
      }
    } else {
      _nll->setData(*theData,false);
    }
    
  } else {
    //reusing an NLL, but must ensure we swap in the data ...
    _nll->setData(*theData,false);
  }
  
  
  //any floating parameter that does not feature in the pdf or dataset should be held constant ... 
  //if this isn't done then saw getError method in TRooAbsH1 ends up putting variables like stat gamma's of validation regions into nset, which shouldn't be in the nset
  RooArgSet* allPars = _nll->getVariables();
  RooArgSet* allFloatVars = static_cast<RooArgSet*>(allVars().selectByAttrib("Constant",kFALSE));
  
  //check if NominalParamValues snapshot exists -- if not, create it before this first fit 
  //this lets us restore the fit state at any time
    RooFit::MsgLevel level = RooMsgService::instance().globalKillBelow();
    RooMsgService::instance().setGlobalKillBelow(RooFit::FATAL);
    const RooArgSet* nomParams = getSnapshot("NominalParamValues");
    RooMsgService::instance().setGlobalKillBelow(level);

  if(nomParams==0) {
    saveSnapshot("NominalParamValues",*allPars);
  } else if(sOpt.Contains("r")) {
    //reload initial values (and errors) before fit ... important though that we do not tamper with the const pars though!!
    std::unique_ptr<RooAbsCollection> floatPars(allPars->selectByAttrib("Constant",kFALSE));
    
    //have to do the loop ourself because the errors arent propagated in assignment
    RooFIter itr = floatPars->fwdIterator();
    RooAbsArg* arg(0);
    while( (arg = itr.next()) ) {
      RooRealVar* vinit = dynamic_cast<RooRealVar*>(nomParams->find(*arg));
      if(!vinit) continue;
      RooRealVar* v = dynamic_cast<RooRealVar*>(arg);
      if(!v) continue;
      v->setVal(vinit->getVal()); v->setError( vinit->getError() ); 
      if( vinit->hasAsymError() ) {
        v->setAsymError( vinit->getErrorLo(), vinit->getErrorHi() );
      }
    }
    
  }
  
  if(globalObservables) allVars() = *globalObservables; //sets the global observables;
  
  
  
  allFloatVars->remove(*allPars);allFloatVars->remove(*theData->get(),true,true);
  if(allFloatVars->getSize()) {
    if(!sOpt.Contains("q")) Info("fitTo","Temporarily setting the following parameters constant: %s", allFloatVars->contentsString().c_str());
    TRooFit::setAttribAll(*allFloatVars,"Constant",kTRUE);
  }
  delete allPars; 
  
  if(!kDisabledForcedRecommendedOptions) TRooFit::setRecommendedDefaultOptions();
  RooFitResult* result = TRooFit::minimize(_nll, true, doHesse);
 
  result->SetName(Form("fitTo_%s",theData->GetName()));
  result->SetTitle(fitTitle);
  
  if(internalData) _nll->setAttribute(result->GetName()); //used to track which nll corresponds to which fit
  
  if(reducedData && !internalData) delete reducedData;
  
  //before returning we will override _minLL with the actual NLL value ... offsetting could have messed up the value
  result->_minNLL = _nll->getVal();
  
  if(internalData && !sOpt.Contains("q")) result->Print();
  
  //purpose of the "n" option is to "not save" the nll in the nll list when creating nll for internal data
  //so if nll was created for internal data (happens if r option specified or nll didnt exist
  if((!internalData || (internalData&&!fNll.find(*_nll))) && !_nllToReuse) delete _nll;
  
  if(_nllToReuse && !(*_nllToReuse)) *_nllToReuse = _nll; //return pointer to _nll
  
  //refloat the float vars ...
  if(allFloatVars->getSize()) {
    //these parameters into the constPars list in fact ... to protect getError calls from trying to normalize over them (putting them in the nset) ..
    //const_cast<RooArgList&>(result->constPars()).addClone(*allFloatVars);
    TRooFit::setAttribAll(*allFloatVars,"Constant",kFALSE);
    const_cast<RooArgList&>(result->floatParsInit()).addClone(*allFloatVars);
  }
  
  delete allFloatVars;
  
  
  return result;
  
}

RooAbsReal* TRooWorkspace::getFitNll(const char* fitName) {
  //This method looks for the nll function corresponding to the given fit name
  
  TString sFitName = (fitName) ? fitName : fCurrentFit.Data();
  
  if(sFitName=="") return 0;
  
  std::unique_ptr<RooAbsCollection> possibleNll( fNll.selectByAttrib(sFitName,true) );

  if(possibleNll->getSize()==0) return 0;
  else if(possibleNll->getSize()==1) return static_cast<RooAbsReal*>(possibleNll->first());
  else {
    Error("getFitNll","Found %d possible nll functions for %s ?? ",possibleNll->getSize(),sFitName.Data());
    return 0;
  }
}

//compute impact using the current fit ...
double TRooWorkspace::impact(const char* poi, const char* np, bool positive) {
  RooAbsReal* _nll = getFitNll();
  if(!_nll) return 0;
  
  RooFitResult* out = getFit();
  if(!out) return 0;
  
  std::unique_ptr<RooArgSet> globalFloatPars( _nll->getObservables(out->floatParsInit()) ); 
  
  RooRealVar* arg = (RooRealVar*)globalFloatPars->find(np);
  if(!arg) return 0;
  
  RooRealVar* poiArg = dynamic_cast<RooRealVar*>(out->floatParsFinal().find(poi));
  if(!poiArg) return 0;
  
  
  RooRealVar* parInNLL = arg;
  RooRealVar* parInGlobalFit = ((RooRealVar*)out->floatParsFinal().find(arg->GetName()));
  
  parInNLL->setConstant(1);
  parInNLL->setVal( parInGlobalFit->getVal() + ((!positive) ? parInGlobalFit->getErrorLo() : parInGlobalFit->getErrorHi()) ); //set to desired value
  //rerun the fit ..
  //Info("impact","Computing impact on %s due to %s",poi,np);
  
  RooFitResult* impactFit = TRooFit::minimize(_nll);
  double postfitImpact = ((RooRealVar*)impactFit->floatParsFinal().find(poi))->getVal() - poiArg->getVal();
  
  *globalFloatPars = out->floatParsFinal(); //restore final state (or should it be initial state?) for global fit (includes restoring non-constant state)
  
  
  delete impactFit;
  return postfitImpact;
  
}

void TRooWorkspace::impact(const char* impactPar, float correlationThreshold) {
  //computes impact on given parameter, or parameter of interest
  
  TString parName;
  
  if(impactPar==0 && set("poi")->getSize()==1)  parName=set("poi")->first()->GetName();
  else if(impactPar) parName = impactPar;
  else {
    return;
  }
  
  RooFitResult* out = getFit();
  if(!out) return;
  
  if(impactPar==0 || out->floatParsInit().find(impactPar)==0) return;
  
  
  TString sOpt("");
  
  TVirtualPad* pad = gPad;
  if(!pad) {
    gROOT->MakeDefCanvas();
    pad = gPad;
  }
  
  
     
     
      
  
  
  
  int myIdx = out->floatParsFinal().index(parName)+1;
  std::vector<std::pair<std::string,double>> vals;
  int count(0);
  for(int i=1;i<=out->floatParsFinal().getSize();i++) {
    if(i==myIdx) continue; //don't plot self correlation/covariance
    if( fabs(out->correlation(myIdx-1,i-1)) < correlationThreshold ) continue;
    vals.push_back(  std::make_pair(out->floatParsFinal().at(i-1)->GetName(),out->correlation(myIdx-1,i-1) ) );
    count++;
  }

  std::sort(vals.begin(),vals.end(), [](auto &left, auto &right) { return fabs(left.second) > fabs(right.second); });
  
  int nBins = vals.size();
  
  if(nBins==0) {
    Error("impact","No correlations were above the threshold %g",correlationThreshold);
    return;
  }
  
  TH1D* impactHist = new TH1D("impact","#theta = #hat{#theta}+#Delta#hat{#theta}",nBins,0,nBins);
  impactHist->SetBarWidth(0.9);impactHist->SetBarOffset(0.05);
  impactHist->SetFillColor(4);
  impactHist->SetDirectory(0);
  impactHist->GetYaxis()->SetTitle(Form("#Delta%s",out->floatParsInit().find(impactPar)->GetTitle()));
  
  //create a copy histogram and add it ...
  TH1* copyHist = static_cast<TH1*>(impactHist->Clone(impactHist->GetName())); copyHist->SetDirectory(0);
  copyHist->SetTitle("#theta = #hat{#theta}-#Delta#hat{#theta}");
  copyHist->SetFillColor(kCyan);
  impactHist->GetListOfFunctions()->Add(copyHist,"b same");
  
  impactHist->SetBit(kCanDelete);
  impactHist->Draw("B");
  
  TLegend* myLegend = (TLegend*)GetLegend()->Clone("legend");
  myLegend->SetBit(kCanDelete); //so that it is deleted when pad cleared
  myLegend->AddEntry(impactHist,impactHist->GetTitle(),"F");
  myLegend->AddEntry(copyHist,copyHist->GetTitle(),"F");
  
  
  myLegend->SetY1( myLegend->GetY2() - myLegend->GetNRows()*myLegend->GetTextSize() );
  myLegend->ConvertNDCtoPad();
  myLegend->Draw();
  
  float maxVal=0;
  for(int i=0;i<impactHist->GetNbinsX();i++) {
    RooAbsArg* arg = out->floatParsFinal().find(vals[i].first.c_str());
    //to compute e.g. post-fit impact due to lumi uncert, shift and fix par before redoing global fit ...
    //std::cout << "correlation = " << out->correlation(myIdx,out->floatParsFinal().index(arg->GetName())) << std::endl;
    for(int j=-1;j<2;j+=2) { 
  
      
      double postfitImpact = impact(parName, arg->GetName(), (j>0) );

      if(j>0) {
        impactHist->SetBinContent(i+1,postfitImpact);
        impactHist->GetXaxis()->SetBinLabel(i+1,arg->GetTitle());
      } else {
        copyHist->SetBinContent(i+1,postfitImpact);
      }
      
      if(fabs(postfitImpact) > maxVal) maxVal = fabs(postfitImpact);
    
      impactHist->SetAxisRange(-maxVal*1.1,maxVal*1.1,"Y");
      pad->Modified(1);
      pad->Update();
    
    }
  }
       
  
  
  
  
  

  

  
  if(!sOpt.Contains("same")) {
    TLatex latex;latex.SetNDC();latex.SetTextSize(myLegend->GetTextSize());
    Double_t xPos = gPad->GetLeftMargin()+12./gPad->GetCanvas()->GetWw();
    Double_t yPos = 1. - gPad->GetTopMargin() -12./gPad->GetCanvas()->GetWh() - latex.GetTextSize();
    for(auto label : fLabels) {
      latex.DrawLatex(xPos,yPos,label);
      yPos -= latex.GetTextSize();
    }
  }

  pad->Update();
  
}

RooFitResult* TRooWorkspace::fitTo(const char* dataName, Option_t* opt, const char* fitName) {
  RooAbsData* theData = data((dataName)?dataName:fCurrentData.Data());
  
  if(!theData) {
    Error("fitTo","Data %s is not in the workspace",dataName);
    return 0;
  }
  
  fCurrentData = theData->GetName(); //switch to this data
  
  model(); //call this method just the bring globalObservables snapshot into existence if necessary
  RooFitResult* result = fitTo(theData,getSnapshot(Form("%s_globalObservables",theData->GetName())),opt);
  if(fitName) result->SetName(fitName);
  import(*result,true/*overwrites existing*/); //FIXME: the statusHistory is not imported because RooFitResult Clone doesnt copy

  
  RooFitResult* out = loadFit(result->GetName());
  out->_statusHistory = result->_statusHistory; //HACK!!
  fCurrentFitResult->_statusHistory = result->_statusHistory; //HACK!!
  
  delete result;
  
  
  
  
  
  //delete nll;
  
  //if(reducedData) delete reducedData;
   
  return out;
}

RooFitResult* TRooWorkspace::loadFit(const char* fitName, bool prefit) {
  if(fitName==nullptr) {
    //interpretted as loading the prefit state as saved in NominalParamValues 
    loadSnapshot("NominalParamValues");
    fCurrentFit = "";
    if(fCurrentFitResult) delete fCurrentFitResult;
    fCurrentFitResult = 0;
    return 0;
  }

  RooFitResult* result = dynamic_cast<RooFitResult*>(obj(fitName));
  if(!result) {
    Error("loadFit","Could not find fit %s",fitName);
    return 0;
  }
  
  allVars() = result->constPars();
  if(prefit) {
    allVars() = result->floatParsInit();
    fCurrentFitIsPrefit = true;
  } else {
    allVars() = result->floatParsFinal();
    fCurrentFitIsPrefit = false;
  }
  
  fCurrentFit = fitName;
  if(fCurrentFitResult) delete fCurrentFitResult;
  fCurrentFitResult = new TRooFitResult( result );

  // remove from the current dir to avoid polluting it (is this safe??)
  fCurrentFitResult->removeFromDir(fCurrentFitResult);
  result->removeFromDir(result);
  
  fCurrentFitResult->_statusHistory = result->_statusHistory; //HACK!!
  
  //constPars are expunged from the fCurrentFitResult, to help speed up printout of yields (avoids recopying const pars)
  const_cast<RooArgList&>(fCurrentFitResult->constPars()).removeAll();
  
  return result;
  
  
}

Bool_t TRooWorkspace::import(std::pair<RooAbsData*,RooArgSet*> dataAndGobs) {
  if(dataAndGobs.first==0) return true;
  
  if(data(dataAndGobs.first->GetName())) {
    Error("import","Cannot import data. %s already exists",dataAndGobs.first->GetName());
    return true;
  }
  
  if( import(*dataAndGobs.first) ) return true;

  // cleanup imported datasets from directory
  if(auto d = dynamic_cast<RooDataSet*>(data(dataAndGobs.first->GetName())); d) {
      d->removeFromDir(d);
  }
  
  //import the gobs as a snapshot ...
  if(!dataAndGobs.second) return false;
  
  saveSnapshot(TString::Format("%s_globalObservables",dataAndGobs.first->GetName()),*dataAndGobs.second,true);
  
  return false;
  
  
}


TLegend* TRooWorkspace::GetLegend() {
  if(!fLegend) { 
    if(gPad) fLegend = new TLegend(0.5,1.-gPad->GetTopMargin()-0.2,1.-gPad->GetRightMargin(),1.-gPad->GetTopMargin() - 0.03); 
    else fLegend = new TLegend(0.5,1.-gStyle->GetPadTopMargin()-0.2,1.-gStyle->GetPadRightMargin(),1.-gStyle->GetPadTopMargin() - 0.03); 
    fLegend->SetLineWidth(0);
    fLegend->SetFillStyle(0);
    fLegend->SetTextSize(gStyle->GetTextSize()*0.75 / (1. - fRatioHeight) );
  }
  return fLegend;
}

//draw a channel's stack and overlay the data too
void TRooWorkspace::channelDraw(const char* channelName, const char* opt, const TRooFitResult& res) {

  RooMsgService::instance().getStream(RooFit::INFO).removeTopic(RooFit::NumIntegration); //stop info message every time


  TVirtualPad* pad = gPad;
  if(!pad) {
    gROOT->MakeDefCanvas();
    pad = gPad;
  }

  TString sOpt(opt);
  sOpt.ToLower();
  
  if(fCurrentFitIsPrefit && !sOpt.Contains("init")) sOpt += "init";

  

  pad->SetName(channel(channelName)->GetName());pad->SetTitle(channel(channelName)->GetTitle());
  
  TVirtualPad* ratioPad = 0;
  if(fRatioHeight>0.) {
  
    if(!sOpt.Contains("same")) {
      //drawing a ratio plot has been requested ... divide the pad into two ...
      TPad* mainPad = new TPad(pad->GetName(),pad->GetTitle(),0,fRatioHeight,1,1);
      mainPad->SetNumber(1);mainPad->SetBorderMode(0);
      mainPad->SetBottomMargin(0.01); mainPad->SetTopMargin( pad->GetTopMargin()/(1.-fRatioHeight) );
      
      ratioPad = new TPad(Form("%s_ratio",pad->GetName()),pad->GetTitle(),0,0,1,fRatioHeight);
      ((TPad*)ratioPad)->SetNumber(2);ratioPad->SetBorderMode(0);
      ratioPad->SetTopMargin(0.01); ratioPad->SetBottomMargin( pad->GetBottomMargin()/fRatioHeight );
      
      mainPad->Draw();
      ratioPad->Draw();
      mainPad->cd();
      pad = mainPad;
    } else {
      //assumes correct pads already exist
      TVirtualPad* mainPad = pad->GetPad(1);
      mainPad->cd();
      ratioPad = pad->GetPad(2);
      pad = mainPad;
    }
  }
  
  int nPrimitives=0;
  if(sOpt.Contains("same")) {
    //store how many primitives there are in the pad, because any extra primitives we get 
    //from drawing the channel we will process in a moment ..
    nPrimitives = pad->GetListOfPrimitives()->GetEntries();
  }
  
  channel(channelName)->Draw(sOpt,res);
  
  TLegend* myLegend = 0;
  if(!sOpt.Contains("same")) { 
    myLegend = (TLegend*)GetLegend()->Clone("legend");
    myLegend->SetBit(kCanDelete); //so that it is deleted when pad cleared
    myLegend->SetTextSize(myLegend->GetTextSize()*(1.-fRatioHeight));
  } else {
    for(int i=0;i<pad->GetListOfPrimitives()->GetEntries();i++) {
      TObject* obj = pad->GetListOfPrimitives()->At(i);
      if(obj->IsA()==TLegend::Class()) {
        myLegend = (TLegend*)obj;
        break;
      }
    }
  }
  
  if(function(channelName)->getAttribute("isValidation")) {
    gPad->SetFillColor(kGray);
    if(ratioPad) ratioPad->SetFillColor(kGray);
  }
  
  
  TGraphAsymmErrors* dataGraph = 0;
  if(!sOpt.Contains("same")) {
    fDummyHists[channelName]->Reset();
    if(data(fCurrentData)) {
      //determine observables for this channel ...
      RooArgSet* chanObs = function(channelName)->getObservables(data(fCurrentData));
      RooArgList l(*chanObs); delete chanObs;
      fDummyHists[channelName]->SetMinimum(1e-9); //resets minimum
      fDummyHists[channelName]->SetMarkerStyle(20);
      fDummyHists[channelName]->SetLineColor(kBlack);
      
      
      if(fDummyHists[channelName]->GetDimension()==1) {
        //Will fill histogram manually so that we can track the weighted average position in each bin too 
        
        TH1* theHist = fDummyHists[channelName];
        
        //this hist will get filled with w*x to track weighted x position per bin
        TH1* xPos = (TH1*)theHist->Clone("xPos");
        xPos->Reset();
        TH1* xPos2 = (TH1*)theHist->Clone("xPos2");
        xPos2->Reset();
        
        auto theData = data(fCurrentData);
        
        const RooAbsReal* xvar = static_cast<RooAbsReal*>(theData->get()->find(l.at(0)->GetName()));
        const RooCategory* catvar = static_cast<RooCategory*>(theData->get()->find(fChannelCatName));
        
        int catIndex = cat(fChannelCatName)->lookupType(channelName)->getVal();
        
        int nevent = theData->numEntries();
        for(int i=0;i<nevent;i++) {
          theData->get(i);
          if(catvar->getIndex() != catIndex) continue;
          
          xPos->Fill( xvar->getVal() , xvar->getVal()*theData->weight() );
          xPos2->Fill( xvar->getVal() , pow( xvar->getVal(), 2 )*theData->weight() );
          theHist->Fill( xvar->getVal(), theData->weight() );
        }
        
        xPos->Divide(theHist);xPos2->Divide(theHist);
        
        for(int i=1;i<=theHist->GetNbinsX();i++) {
            // FIXME: Should divide numbers in the graph, otherwise error bars are wrong.
            if (sOpt.Contains("v")) { theHist->SetBinContent(i, theHist->GetBinContent(i)
            /theHist->GetXaxis()->GetBinWidth(i));
            }
            theHist->SetBinError(i,sqrt(theHist->GetBinContent(i)));
        }

        
        dataGraph = new TGraphAsymmErrors;
        dataGraph->SetName(theHist->GetName());
        *static_cast<TAttMarker*>(dataGraph) = *static_cast<TAttMarker*>(theHist);
        *static_cast<TAttLine*>(dataGraph) = *static_cast<TAttLine*>(theHist);
        dataGraph->SetBit(kCanDelete);
        //update the x positions to the means for each bin and use poisson asymmetric errors for data ..
        for(int i=0;i<theHist->GetNbinsX();i++) {
          if(theHist->GetBinContent(i+1)) {
            double val = theHist->GetBinContent(i+1);
            dataGraph->SetPoint(dataGraph->GetN(), xPos->GetBinContent(i+1), val);
            
            //x-error will be the (weighted) standard deviation of the x values ...
            double xErr = sqrt( xPos2->GetBinContent(i+1) - pow(xPos->GetBinContent(i+1),2) );
            
            dataGraph->SetPointError(dataGraph->GetN()-1, xErr, xErr, val - 0.5*TMath::ChisquareQuantile(TMath::Prob(1,1)/2.,2.*(val)), 0.5*TMath::ChisquareQuantile(1.-TMath::Prob(1,1)/2.,2.*(val+1)) - val );
          }
        }
        dataGraph->Draw("pZ0 same");
        
        delete xPos; delete xPos2;
        
      } else {
        data(fCurrentData)->fillHistogram(fDummyHists[channelName], l, Form("%s==%s::%s",fChannelCatName.Data(),fChannelCatName.Data(),channelName));
        //update all errors to usual poisson
        for(int i=1;i<=fDummyHists[channelName]->GetNbinsX();i++) fDummyHists[channelName]->SetBinError(i,sqrt(fDummyHists[channelName]->GetBinContent(i)));
        fDummyHists[channelName]->Draw("p EX0 same");
      }
      
      
      
      myLegend->AddEntry(fDummyHists[channelName],data(fCurrentData)->GetTitle(),"pEX0");
      
    }
    
    THStack* myStack = (THStack*)gPad->GetListOfPrimitives()->FindObject(Form("%s_stack",channel(channelName)->GetName()));
    if(myStack) {
      //add stack entries to legend
      TList* l = myStack->GetHists();
      if (l) {
          for (int i = l->GetEntries() - 1; i >= 0; i--) { //go in reverse order
              if (l->At(i)->TestBit(TRooFit::kAllowNegative)) continue; // don't add the 'negative'stack hists
              myLegend->AddEntry(l->At(i), l->At(i)->GetTitle(), "f");
          }
      }
    }
    //adjust legend size based on # of rows 
    myLegend->SetY1( myLegend->GetY2() - myLegend->GetNRows()*myLegend->GetTextSize() );
    myLegend->ConvertNDCtoPad();
    myLegend->Draw();
  } else {
    //how many primitives do we need to process ?
    TString errName = TString::Format("%s_error",channelName);bool doneErr=false;
    for(int i=gPad->GetListOfPrimitives()->GetEntries()-1;i>=nPrimitives;i--) { //go in reverse order because error appears after nominal in primitives list
      TObject* obj = gPad->GetListOfPrimitives()->At(i);
      if(errName == obj->GetName()) {
        doneErr=true;
        myLegend->AddEntry(obj, (function(channelName)->getStringAttribute("sameTitle")) ? function(channelName)->getStringAttribute("sameTitle") : function(channelName)->GetTitle(), "fl");
      } else if(strcmp(channelName,obj->GetName())==0 && !doneErr) {
        myLegend->AddEntry(obj, (function(channelName)->getStringAttribute("sameTitle")) ? function(channelName)->getStringAttribute("sameTitle") : function(channelName)->GetTitle(), "l");
      } 
    }
  
    /*
    if(sOpt.Contains("init") && sOpt.Contains("hist") && myLegend) {
      //user trying to display prefit ... add this to legend ...
      myLegend->AddEntry(gPad->GetListOfPrimitives()->Last()  ,"Prefit", "l");
    }
    */
    //adjust legend size based on # of rows 
    myLegend->SetY1NDC( myLegend->GetY2NDC() - myLegend->GetNRows()*myLegend->GetTextSize() );
    myLegend->ConvertNDCtoPad();
  }
  
  

  
  if(!sOpt.Contains("same")) {
    TLatex latex;latex.SetNDC();latex.SetTextSize(myLegend->GetTextSize());
    Double_t xPos = gPad->GetLeftMargin()+12./gPad->GetCanvas()->GetWw();
    Double_t yPos = 1. - gPad->GetTopMargin() -12./gPad->GetCanvas()->GetWh() - latex.GetTextSize();

    for(auto label : fLabels) {
        int i = label.Index("[["); int j = label.Index("]]");
        if (j > i+2) {
            auto a = function(TString(label(i+2,j-i-2)));
            if (a) {
                RooArgSet* s = (RooArgSet*)allVars().snapshot();
                allVars() = res.floatParsFinal();
                allVars() = res.constPars();
                label = TString(label(0,i)) + Form("%g",a->getVal()) + TString(label(j+2,
                        label.Length()));
                allVars() = *s;
                delete s;
            }
        }
      latex.DrawLatex(xPos,yPos,label);
      yPos -= latex.GetTextSize();
    }
    latex.DrawLatex(xPos,yPos,channel(channelName)->GetTitle());
    yPos -= latex.GetTextSize();
    if(fCurrentFit!="" && !fCurrentFitIsPrefit && !sOpt.Contains("init")) {
        int fstat = getFit(fCurrentFit)->status();
      if(fstat%1000!=0) {
          latex.SetTextColor(kRed);
      } else if(fstat>0) {
          latex.SetTextColor(kOrange);
      }
      latex.DrawLatex(xPos,yPos,"Post-Fit");
      latex.SetTextColor(kBlack);
    }
    else latex.DrawLatex(xPos,yPos,"Pre-Fit");
  }

  pad->Update();

  if(!sOpt.Contains("same")) {

    //create space for the legend by adjusting the range on the stack ...
    double currMax = pad->GetUymax();
    double currMin = pad->GetUymin();
    
    if(pad->GetLogy()) { //if was log, then get log values back!
      currMax = std::pow(10,currMax);
      currMin = std::pow(10,currMin);
    }
    
    if(data(fCurrentData)) {
      double maxVal = fDummyHists[channelName]->GetBinContent(fDummyHists[channelName]->GetMaximumBin());
      double minVal = fDummyHists[channelName]->GetBinContent(fDummyHists[channelName]->GetMinimumBin());
      
      if(currMax < maxVal) currMax=maxVal;
      if(currMin > minVal) currMin=minVal;
    }
    
    if(gPad->GetLogy()) {
      currMin = std::log10(std::max(currMin,1e-6));
      currMax = std::log10(currMax);
    }
    
    //double yScale = (currMax-currMin)/(1.-gPad->GetTopMargin()-gPad->GetBottomMargin());
    
    //want new max to give a newYscale where currMax is at shifted location 
    double newYscale = (currMax-currMin)/(myLegend->GetY1NDC()-pad->GetBottomMargin());
    
    currMax = currMin + newYscale*(1.-pad->GetTopMargin()-pad->GetBottomMargin());
    
    if(gPad->GetLogy()) {
      currMin = std::pow(10,currMin);
      currMax = std::pow(10,currMax);
    }
    
    //currMax += yScale*(myLegend->GetY2NDC()-myLegend->GetY1NDC());
    
    channel(channelName)->GetYaxis()->SetRangeUser(currMin,currMax);
    pad->Modified(1);
    pad->Update();
  }
  
  if(ratioPad) {
    
    
    TH1* nominalHist = dynamic_cast<TH1*>(pad->GetListOfPrimitives()->FindObject(channelName));
    TH1* errorHist = dynamic_cast<TH1*>(pad->GetListOfPrimitives()->FindObject(Form("%s_error",channelName)));

    
    if(!nominalHist) {
      Error("channelDraw","Could not find nominal histogram for channel %s",channelName);
      return;
    }
    
    //clone nominalHist and remove error, so we can use it for dividing ...
    TH1* nominalHistNoErrors = (TH1*)nominalHist->Clone("nomHist");
    TRooFit::RemoveErrors(nominalHistNoErrors);
    
    
    if(sOpt.Contains("same")) {
      //draw the extra primitives from the main pad, dividing them all by nominal hist first ... 
      ratioPad->cd();
      for(int i=nPrimitives;i<pad->GetListOfPrimitives()->GetEntries();i++) {
        TH1* h = dynamic_cast<TH1*>(pad->GetListOfPrimitives()->At(i));
        if(!h) continue; //only draw histogram ...
        TH1* rh = (TH1*)h->Clone(Form("%sRatio",h->GetName()));
        //need to get the draw option associated to this object, which we can only get from the link, so have to go through the link instead ..
        rh->SetOption(h->GetOption());
        TObjLink* lnk = pad->GetListOfPrimitives()->FirstLink();
        while(lnk) {
          if(lnk->GetObject()==pad->GetListOfPrimitives()->At(i)) { h->SetOption( lnk->GetOption() ); break; }
          lnk = lnk->Next();
        }
        rh->SetBit(kCanDelete);
        rh->Divide(nominalHistNoErrors);
        rh->Draw(h->GetOption());
      }
      
      
    } else {
    
      TGraphAsymmErrors* dataGraphRatio = 0;
      TH1* dataRatio = (TH1*)fDummyHists[channelName]->Clone("data");
      dataRatio->SetDirectory(0);
      TH1* errRatio = (errorHist) ? (TH1*)errorHist->Clone("errRatio") : (TH1*)nominalHistNoErrors->Clone("errRatio");
      errRatio->SetDirectory(0);
      TString s(errRatio->GetOption()); s.ReplaceAll("same","");
      errRatio->SetOption(s);
      if (errRatio->GetNbinsX() != nominalHist->GetNbinsX()) {
          // happens in smooth_curve scenario
          errRatio->SetBins(nominalHist->GetNbinsX(),nominalHist->GetXaxis()->GetXbins()->GetArray());
      }
      errRatio->SetMinimum(0.001);
      errRatio->SetMaximum(1.999);
      errRatio->GetYaxis()->SetNdivisions(5,0,0);
      errRatio->GetYaxis()->SetTitle("Data / Pred.");
      errRatio->GetYaxis()->SetTitleSize( errRatio->GetYaxis()->GetTitleSize() * (1.-fRatioHeight) / fRatioHeight );
      errRatio->GetYaxis()->SetLabelSize( errRatio->GetYaxis()->GetLabelSize() * (1.-fRatioHeight) / fRatioHeight );
      errRatio->GetXaxis()->SetTitleSize( errRatio->GetXaxis()->GetTitleSize() * (1.-fRatioHeight) / fRatioHeight );
      errRatio->GetXaxis()->SetLabelSize( errRatio->GetXaxis()->GetLabelSize() * (1.-fRatioHeight) / fRatioHeight );
      errRatio->GetYaxis()->SetTitleOffset( errRatio->GetYaxis()->GetTitleOffset() * fRatioHeight / (1.-fRatioHeight) );
      
      errRatio->SetStats(false);errRatio->SetBit(TH1::kNoTitle);
      
      if(kShowSignificance) {
        dataRatio->SetMinimum(); //need to remove minimum since significance can go negative
        //calculate significance of data ...
        for(int i=1;i<=dataRatio->GetNbinsX();i++) {
          dataRatio->SetBinContent(i, TRooFit::significance( dataRatio->GetBinContent(i), nominalHist->GetBinContent(i), (!errorHist) ? 0 : errorHist->GetBinError(i)/nominalHist->GetBinContent(i),  (!errorHist) ? 0 : errorHist->GetBinError(i)/nominalHist->GetBinContent(i) ) );
          dataRatio->SetBinError(i,0);
        }
      
        errRatio->Reset();
        errRatio->GetYaxis()->SetTitle("signif. / #sigma");
        errRatio->SetMinimum(-5);
        errRatio->SetMaximum(5);
        dataRatio->SetLineWidth(2);
        
      } else {
        errRatio->Divide(nominalHistNoErrors);
        dataRatio->Divide(nominalHistNoErrors);
        
        if(dataGraph) {
          //use dataGraph instead ...
          delete dataRatio; dataRatio = 0;
          dataGraphRatio = (TGraphAsymmErrors*)dataGraph->Clone("data");
          for(int i=0;i<dataGraphRatio->GetN();i++) {
            double bc = nominalHistNoErrors->GetBinContent( nominalHistNoErrors->FindFixBin( dataGraphRatio->GetX()[i] ) );
            dataGraphRatio->SetPoint(i, dataGraphRatio->GetX()[i], dataGraphRatio->GetY()[i] / bc );
            dataGraphRatio->SetPointEYhigh(i,  dataGraphRatio->GetErrorYhigh(i) / bc );
            dataGraphRatio->SetPointEYlow(i,  dataGraphRatio->GetErrorYlow(i) / bc );
          }
        }
      
      }
      
      
      //need to construct ratio plots and draw them ...
      ratioPad->cd();ratioPad->SetGridy();
      
      errRatio->SetBit(kCanDelete);errRatio->Draw();
      if(dataRatio) { dataRatio->SetBit(kCanDelete);dataRatio->Draw((kShowSignificance) ? "hist same" : "p e0X0 same"); }
      if(dataGraphRatio) { dataGraphRatio->SetBit(kCanDelete);dataGraphRatio->Draw("pZ0 same"); }
      
      
      
    }
    
    delete nominalHistNoErrors; //done with this now ...
    
    
    ratioPad->Modified(1);
    ratioPad->Update();
    
  }
  
}

Bool_t TRooWorkspace::writeToFile(const char* fileName, Bool_t recreate) {
  //ensure all staged channels are imported ...
  /*std::unique_ptr<TIterator> itr( fStagedChannels.createIterator() );
  TObject* obj;
  while( (obj = itr->Next()) ) {
    import(*static_cast<RooAbsPdf*>(obj));
  }
  fStagedChannels.removeAll();
  */
  return RooWorkspace::writeToFile(fileName,recreate);
  
}


void TRooWorkspace::Draw(Option_t* option, const TRooFitResult& res) {
  //draws each channel
  TVirtualPad* pad = gPad;
  if(!pad) {
    gROOT->MakeDefCanvas();
    pad = gPad;
  }
  
  TString sOpt(option);
  sOpt.ToLower();
  
  if(sOpt.Contains("pull") || sOpt.Contains("cor")) {
    //just draw the pull plot of the fit result ..
    RooArgSet* visibleArgs = static_cast<RooArgSet*>(allVars().selectByAttrib("hidden",kFALSE));
    RooArgList* l = static_cast<RooArgList*>(fCurrentFitResult->floatParsFinal().selectCommon(*visibleArgs));
    delete visibleArgs;
    if(fCurrentFitResult) fCurrentFitResult->Draw(sOpt,*l);
    delete l;
    return;
  }

  pad->SetTitle(GetTitle());
  

  if(sOpt.Contains("yields")) {
    //create yield plot (integral of each channel) 
    
    int nCat=0;
    TIterator* catIter = cat(fChannelCatName)->typeIterator();
    TObject* c;
    while( (c = catIter->Next()) ) {
        if(!function(c->GetName())->getAttribute("hidden")) nCat++;
    }
    delete catIter;
    
    if(nCat==0) {
      Error("Draw","No channels are visible");
      return;
    }
  
    if(!sOpt.Contains("same")) pad->Clear();
    
    
    pad->SetName(GetName());pad->SetTitle(Form("%s Yield Plot",GetTitle()));
    
    TVirtualPad* ratioPad = 0;
    TVirtualPad* mainPad = pad;
    if(fRatioHeight>0.) {
    
      if(!sOpt.Contains("same")) {
        //drawing a ratio plot has been requested ... divide the pad into two ...
        mainPad = new TPad(pad->GetName(),pad->GetTitle(),0,fRatioHeight,1,1);
        ((TPad*)mainPad)->SetNumber(1);mainPad->SetBorderMode(0);
        mainPad->SetBottomMargin(0.01); mainPad->SetTopMargin( pad->GetTopMargin()/(1.-fRatioHeight) );
        
        ratioPad = new TPad(Form("%s_ratio",pad->GetName()),pad->GetTitle(),0,0,1,fRatioHeight);
        ((TPad*)ratioPad)->SetNumber(2);ratioPad->SetBorderMode(0);
        ratioPad->SetTopMargin(0.01); ratioPad->SetBottomMargin( pad->GetBottomMargin()/fRatioHeight );
        
        mainPad->Draw();
        ratioPad->Draw();
        mainPad->cd();
      } else {
        //assumes correct pads already exist
        mainPad = pad->GetPad(1);
        mainPad->cd();
        ratioPad = pad->GetPad(2);
      }
    }
    
    TH1D* channelYields = new TH1D("yields","Total",nCat,0,nCat);
    channelYields->SetBit(kCanDelete);
    channelYields->SetDirectory(0);
    channelYields->SetMarkerStyle(0);
    channelYields->Sumw2();
    channelYields->GetYaxis()->SetTitle("Events / Channel");
    TH1D* dataYields = data(fCurrentData) ? (TH1D*)channelYields->Clone("data_yields") : 0;
    
    if(dataYields) {
      dataYields->SetMarkerStyle(20);dataYields->SetLineColor(kBlack);
      dataYields->SetDirectory(0);
      dataYields->SetTitle(data(fCurrentData)->GetTitle());
    }
    
    THStack* stackYields = new THStack("yieldsStack","yields stack");
    stackYields->SetBit(kCanDelete);
    
    std::map<TString,TH1D*> sampleYields;
    
    std::unique_ptr<TIterator> catIter2(cat(fChannelCatName)->typeIterator());
    int binCount(0);
    while( (c = catIter2->Next()) ) {
      TRooAbsHStack* chan = channel(c->GetName());
      RooAbsReal* chanFunc = dynamic_cast<RooAbsReal*>(chan);
      if(chanFunc->getAttribute("hidden")) continue;
      binCount++;
      channelYields->GetXaxis()->SetBinLabel(binCount,chanFunc->GetTitle());
      double err; 
      double inte = chan->IntegralAndError(err,res);
      channelYields->SetBinContent(binCount,inte);channelYields->SetBinError(binCount,(err)?err:1e-15);
      if(dataYields) {
        double yield = data(fCurrentData)->sumEntries(Form("%s==%s::%s",fChannelCatName.Data(),fChannelCatName.Data(),c->GetName()));
        dataYields->SetBinContent(binCount,yield);
      }
      
      
      RooFIter fItr = chan->compList().fwdIterator();
      RooAbsArg* arg;
      while( (arg = fItr.next()) ) {
        if(sampleYields.find(arg->GetTitle())==sampleYields.end()) {
          sampleYields[arg->GetTitle()] = (TH1D*)channelYields->Clone(Form("yields_%s",arg->GetTitle()));
          sampleYields[arg->GetTitle()]->Reset();sampleYields[arg->GetTitle()]->SetTitle(arg->GetTitle());
          sampleYields[arg->GetTitle()]->SetDirectory(0);
          sampleYields[arg->GetTitle()]->SetBit(kCanDelete);
          if(dynamic_cast<TRooAbsH1*>(arg)) {
            sampleYields[arg->GetTitle()]->SetFillColor( dynamic_cast<TRooAbsH1*>(arg)->GetFillColor() );
          } else {
            sampleYields[arg->GetTitle()]->SetFillColor( TRooFit::GetColorByName(arg->GetTitle(),true) );
          }
          stackYields->Add(sampleYields[arg->GetTitle()]);
        }
        inte = IntegralAndError(err,arg->GetName(),chan->GetName(),res);
        
        sampleYields[arg->GetTitle()]->SetBinContent(binCount, sampleYields[arg->GetTitle()]->GetBinContent(binCount)+inte);
      }
      
    }
    channelYields->SetMinimum(1e-9);
    channelYields->Draw("axis");
    stackYields->Draw("noclear same");stackYields->GetHistogram()->SetMarkerStyle(0);
    channelYields->Draw("axissame");
    channelYields->SetFillStyle(3005);channelYields->SetFillColor(channelYields->GetLineColor());
    channelYields->Draw("e2same");
    
    TLegend* myLegend = 0;
    if(!sOpt.Contains("same")) { 
      myLegend = (TLegend*)GetLegend()->Clone("legend");
      myLegend->SetBit(kCanDelete); //so that it is deleted when pad cleared
      myLegend->SetTextSize(myLegend->GetTextSize()*(1.-fRatioHeight));
    } else {
      for(int i=0;i<mainPad->GetListOfPrimitives()->GetEntries();i++) {
        TObject* obj = mainPad->GetListOfPrimitives()->At(i);
        if(obj->IsA()==TLegend::Class()) {
          myLegend = (TLegend*)obj;
          break;
        }
      }
    }
    
    TGraphAsymmErrors* dataGraph=0;
    if(dataYields) {
      dataGraph = new TGraphAsymmErrors;
      dataGraph->SetName(dataYields->GetName());
      *static_cast<TAttMarker*>(dataGraph) = *static_cast<TAttMarker*>(dataYields);
      *static_cast<TAttLine*>(dataGraph) = *static_cast<TAttLine*>(dataYields);
      dataGraph->SetBit(kCanDelete);
      //update the x positions to the means for each bin and use poisson asymmetric errors for data ..
      for(int i=0;i<dataYields->GetNbinsX();i++) {
        if(dataYields->GetBinContent(i+1)) {
          double val = dataYields->GetBinContent(i+1);
          dataGraph->SetPoint(dataGraph->GetN(), dataYields->GetXaxis()->GetBinCenter(i+1), val);
          
          double xErr = 0;
          
          dataGraph->SetPointError(dataGraph->GetN()-1, xErr, xErr, val - 0.5*TMath::ChisquareQuantile(TMath::Prob(1,1)/2.,2.*(val)), 0.5*TMath::ChisquareQuantile(1.-TMath::Prob(1,1)/2.,2.*(val+1)) - val );
        }
      }
      dataGraph->Draw("pZ0 same");
      myLegend->AddEntry(dataGraph,data(fCurrentData)->GetTitle(),"pEX0");
    }

    
    
    
    

    //add stack entries to legend
    TList* l = stackYields->GetHists();
    for(int i=l->GetEntries()-1;i>=0;i--) { //go in reverse order
      myLegend->AddEntry(l->At(i),l->At(i)->GetTitle(),"f");
    }
    //adjust legend size based on # of rows 
    myLegend->SetY1( myLegend->GetY2() - myLegend->GetNRows()*myLegend->GetTextSize() );
    myLegend->ConvertNDCtoPad();
    myLegend->Draw();
    
    
    if(!sOpt.Contains("same")) {
      TLatex latex;latex.SetNDC();latex.SetTextSize(myLegend->GetTextSize());
      Double_t xPos = gPad->GetLeftMargin()+12./gPad->GetCanvas()->GetWw();
      Double_t yPos = 1. - gPad->GetTopMargin() -12./gPad->GetCanvas()->GetWh() - latex.GetTextSize();
      for(auto label : fLabels) {
        latex.DrawLatex(xPos,yPos,label);
        yPos -= latex.GetTextSize();
      }
      latex.DrawLatex(xPos,yPos,"Channel Yields");
      yPos -= latex.GetTextSize();
      if(fCurrentFit!="" && !fCurrentFitIsPrefit && !sOpt.Contains("init")) {
        if(getFit(fCurrentFit)->status()%1000!=0) latex.SetTextColor(kRed);
        latex.DrawLatex(xPos,yPos,"Post-Fit");
        latex.SetTextColor(kBlack);
      }
      else latex.DrawLatex(xPos,yPos,"Pre-Fit");
    }

    mainPad->Update();
  
    //create space for the legend by adjusting the range on the stack ...
    double currMax = mainPad->GetUymax();
    double currMin = mainPad->GetUymin();
    
    if(mainPad->GetLogy()) { //if was log, then get log values back!
      currMax = std::pow(10,currMax);
      currMin = std::pow(10,currMin);
    }
    
    if(data(fCurrentData)) {
      double maxVal = dataYields->GetBinContent(dataYields->GetMaximumBin());
      double minVal = dataYields->GetBinContent(dataYields->GetMinimumBin());
      
      if(currMax < maxVal) currMax=maxVal;
      if(currMin > minVal) currMin=minVal;
    }
    
    
    
    if(gPad->GetLogy()) {
      currMin = std::log10(std::max(currMin,1e-6));
      currMax = std::log10(currMax);
    }
    
    //double yScale = (currMax-currMin)/(1.-gPad->GetTopMargin()-gPad->GetBottomMargin());
    
    //want new max to give a newYscale where currMax is at shifted location 
    double newYscale = (currMax-currMin)/(myLegend->GetY1NDC()-mainPad->GetBottomMargin());
    
    currMax = currMin + newYscale*(1.-mainPad->GetTopMargin()-mainPad->GetBottomMargin());
    
    if(gPad->GetLogy()) {
      currMin = std::pow(10,currMin);
      currMax = std::pow(10,currMax);
    }
    
    

    channelYields->GetYaxis()->SetRangeUser(currMin,currMax);
    stackYields->GetYaxis()->SetRangeUser(currMin,currMax);
      mainPad->Modified(1);
      mainPad->Update();
    
    if(!sOpt.Contains("same") && ratioPad && dataYields) {
      
      
      TH1* nominalHist = channelYields;
      TH1* errorHist = channelYields;
      
      
      //clone nominalHist and remove error, so we can use it for dividing ...
      TH1* nominalHistNoErrors = (TH1*)nominalHist->Clone("nomHist");
      TRooFit::RemoveErrors(nominalHistNoErrors);
      
      TGraphAsymmErrors* dataGraphRatio = 0;
      TH1* dataRatio = (TH1*)dataYields->Clone("data");
      dataRatio->SetDirectory(0);
      TH1* errRatio = (errorHist) ? (TH1*)errorHist->Clone("errRatio") : (TH1*)nominalHistNoErrors->Clone("errRatio");
      errRatio->SetDirectory(0);
      TString s(errRatio->GetOption()); s.ReplaceAll("same","");
      errRatio->SetOption(s);
      errRatio->SetMinimum(0.001);
      errRatio->SetMaximum(1.999);
      errRatio->GetYaxis()->SetNdivisions(5,0,0);
      errRatio->GetYaxis()->SetTitle("Data / Pred.");
      errRatio->GetYaxis()->SetTitleSize( errRatio->GetYaxis()->GetTitleSize() * (1.-fRatioHeight) / fRatioHeight );
      errRatio->GetYaxis()->SetLabelSize( errRatio->GetYaxis()->GetLabelSize() * (1.-fRatioHeight) / fRatioHeight );
      errRatio->GetXaxis()->SetTitleSize( errRatio->GetXaxis()->GetTitleSize() * (1.-fRatioHeight) / fRatioHeight );
      errRatio->GetXaxis()->SetLabelSize( errRatio->GetXaxis()->GetLabelSize() * (1.-fRatioHeight) / fRatioHeight );
      errRatio->GetYaxis()->SetTitleOffset( errRatio->GetYaxis()->GetTitleOffset() * fRatioHeight / (1.-fRatioHeight) );
      
      errRatio->SetStats(false);errRatio->SetBit(TH1::kNoTitle);
      
      if(kShowSignificance) {
        //calculate significance of data ...
        for(int i=1;i<=dataRatio->GetNbinsX();i++) {
          dataRatio->SetBinContent(i, TRooFit::significance( dataRatio->GetBinContent(i), nominalHist->GetBinContent(i), errorHist->GetBinError(i)/nominalHist->GetBinContent(i),  errorHist->GetBinError(i)/nominalHist->GetBinContent(i) ) );
          dataRatio->SetBinError(i,0);
        }
      
        errRatio->Reset();
        errRatio->GetYaxis()->SetTitle("signif. / #sigma");
        errRatio->SetMinimum(-5);
        errRatio->SetMaximum(5);
        dataRatio->SetLineWidth(2);
        
      } else {
        errRatio->Divide(nominalHistNoErrors);
        dataRatio->Divide(nominalHistNoErrors);
        
        if(dataGraph) {
          //use dataGraph instead ...
          delete dataRatio; dataRatio = 0;
          dataGraphRatio = (TGraphAsymmErrors*)dataGraph->Clone("data");
          for(int i=0;i<dataGraphRatio->GetN();i++) {
            double bc = nominalHistNoErrors->GetBinContent( nominalHistNoErrors->FindFixBin( dataGraphRatio->GetX()[i] ) );
            dataGraphRatio->SetPoint(i, dataGraphRatio->GetX()[i], dataGraphRatio->GetY()[i] / bc );
            dataGraphRatio->SetPointEYhigh(i,  dataGraphRatio->GetErrorYhigh(i) / bc );
            dataGraphRatio->SetPointEYlow(i,  dataGraphRatio->GetErrorYlow(i) / bc );
          }
        }
        
      }
      
      delete nominalHistNoErrors; //done with this now ...
      
      
      
      //need to construct ratio plots and draw them ...
      ratioPad->cd();ratioPad->SetGridy();
      
      errRatio->SetBit(kCanDelete);errRatio->Draw();
      if(dataRatio) { dataRatio->SetBit(kCanDelete);dataRatio->Draw((kShowSignificance) ? "hist same" : "p e0X0 same"); }
      if(dataGraphRatio) { dataGraphRatio->SetBit(kCanDelete);dataGraphRatio->Draw("pZ0 same"); }
      
      ratioPad->Modified(1);
      ratioPad->Update();
      
    }
    
    delete dataYields;
    pad->cd(0);
    return;
    
  }

  if (!cat(fChannelCatName)) return;

  
  if(!sOpt.Contains("same")) {
    pad->Clear();
  
    int nCat=0;
    TIterator* catIter = cat(fChannelCatName)->typeIterator();
    TObject* c;
    while( (c = catIter->Next()) ) if(!function(c->GetName())->getAttribute("hidden")) nCat++;
    delete catIter;
    
    if(nCat>1) {
      int nRows = nCat/sqrt(nCat);
      int nCols = nCat/nRows;
      if(nRows*nCols < nCat) nCols++;
      if (fNumChannelColumns > 0) {
          nCols = fNumChannelColumns;
          nRows = nCat/nCols;
      }
      pad->Divide(nCols,nRows);
    }
  }
  
  
  TIterator* catIter = cat(fChannelCatName)->typeIterator();
  TObject* c;
  int i=1;
  while( (c = catIter->Next()) ) {
    if(function(c->GetName())->getAttribute("hidden")) continue;
    pad->cd(i++);
    channelDraw(c->GetName(),option,res);
  }
  pad->cd(0);

  delete catIter;

}

//draws all channels, showing how values of channels depend on var
void TRooWorkspace::DrawDependence(const char* _var, Option_t* option) {
  RooRealVar* theVar = var(_var);
  if(!theVar) {
    Error("DrawDependence","%s not found",_var);
    return;
  }

  //draws each channel
  TVirtualPad* pad = gPad;
  if(!pad) {
    gROOT->MakeDefCanvas();
    pad = gPad;
  }
  
  //always draw to the whole canvas ...
  pad->GetCanvas()->cd();
  pad = gPad;
  
  TString sOpt(option);
  sOpt.ToLower();
  if(!sOpt.Contains("same")) {
    pad->Clear();
    
    int nCat=0;
    TIterator* catIter = cat(fChannelCatName)->typeIterator();
    TObject* c;
    while( (c = catIter->Next()) ) if(!function(c->GetName())->getAttribute("hidden")) nCat++;
    delete catIter;
    
    if(nCat>1) {
      int nRows = nCat/sqrt(nCat);
      int nCols = nCat/nRows;
      if(nRows*nCols < nCat) nCols++;
      pad->Divide(nCols,nRows);
    }
  }

  bool doSlice = sOpt.Contains("slice");
  sOpt.ReplaceAll("slice","");
  bool doPull = sOpt.Contains("pull");
  sOpt.ReplaceAll("pull","");
  
  TIterator* catIter = cat(fChannelCatName)->typeIterator();
  TObject* c;
  int i=1;
  while( (c = catIter->Next()) ) {
    TRooAbsHStack* chan = channel(c->GetName());
    RooAbsReal* chanFunc = dynamic_cast<RooAbsReal*>(chan);
    if(chanFunc->getAttribute("hidden")) continue;
    pad->cd(i++);
    if(chanFunc->getAttribute("isValidation")) gPad->SetFillColor(kGray);
    if(!chanFunc->dependsOn(*theVar)) continue; //no dependence
    
    if(doSlice) {
      //perform 3 slices, around current value of parameter, based on range ...
      double curVal = theVar->getVal();
      
      //double maxVal = theVar->getMax();
      //double minVal = theVar->getMin();
      //double minDiff = std::min(maxVal-curVal,curVal-minVal);
      //double step = minDiff/2.;
      
      int cols[5] = {kOrange,kRed,kBlack,kBlue,kCyan};
      std::vector<TH1*> hists;
      for(int j=-1;j<2;j++) {
        //double val = curVal + j*step;
        double val = curVal; if(j<0) val += theVar->getErrorLo(); else if(j>0) val += theVar->getErrorHi();
        TRooFitResult r(TString::Format("%s=%f",_var,val));
        TH1* hist = (TH1*)chan->GetHistogram(&r,false)->Clone(chan->GetName());
        hist->SetDirectory(0);
        hist->SetTitle(Form("%s = %g",_var,val));
        hist->SetLineWidth(1);
        hist->SetLineColor(cols[j+2]);
        hist->SetBit(kCanDelete);
        hists.push_back(hist);
      }
      if(sOpt.Contains("rel")) {
        hists[0]->Divide(hists[1]);hists[2]->Divide(hists[1]);hists[1]->Divide(hists[1]);
        hists[0]->SetMaximum( std::max( hists[0]->GetBinContent(hists[0]->GetMaximumBin()), hists[1]->GetBinContent(hists[1]->GetMaximumBin()) )*1.1 );
        hists[0]->SetMinimum( std::min( hists[0]->GetBinContent(hists[0]->GetMinimumBin()), hists[1]->GetBinContent(hists[1]->GetMinimumBin()) )*0.9 );
        hists[0]->GetYaxis()->SetTitle("Relative Variation");
      }
      //hists[0]->Divide(hists[2]);hists[1]->Divide(hists[2]);hists[3]->Divide(hists[2]);hists[4]->Divide(hists[2]);
      //hists[2]->Divide(hists[2]);
      for(unsigned int j=0;j<hists.size();j++) {
        hists[j]->Draw((j==0)?"":"same");
      }
      
      
    } else if(doPull) {
     
     //draw the current and (if a fit is loaded, the prefit) pulls for the given variable ... defined here as (prediction-data)/sigma_data
     TH1* dataHist = (TH1*)fDummyHists[c->GetName()]->Clone("dataHist");
     RooArgSet* chanObs = chanFunc->getObservables(data(fCurrentData));
     RooArgList l(*chanObs); delete chanObs;
     data(fCurrentData)->fillHistogram(dataHist, l, Form("%s==%s::%s",fChannelCatName.Data(),fChannelCatName.Data(),c->GetName()));
     
     double varVals[6] = {theVar->getVal(),theVar->getVal()+theVar->getErrorHi(),theVar->getVal()+theVar->getErrorLo(),0,0,0};
     RooRealVar* varInit = 0;
     if(getFit()) {
      varInit = dynamic_cast<RooRealVar*>(getFit()->floatParsInit().find(theVar->GetName()));
      if(varInit) {
        varVals[3] = varInit->getVal(); varVals[4] = varInit->getVal()+varInit->getErrorHi(); varVals[5] = varInit->getVal()+varInit->getErrorLo();
      }
     }
     int cols[6] = {kBlack,kBlue,kRed,kBlack,kBlue,kRed};
     for(int j=0;j<6;j++) {
      if(j>2 && !varInit) continue; //no prefit values available, so do not draw ..
      
      TRooFitResult r(TString::Format("%s=%f",_var,varVals[j]));
      TH1* myHist = (TH1*)chan->GetHistogram(&r,false)->Clone(chan->GetName());
      
      myHist->SetDirectory(0);
      myHist->SetTitle(Form("%s = %g",_var,varVals[j]));
      myHist->SetLineWidth(1);
      myHist->SetLineColor(cols[j]);
      myHist->SetLineStyle(1+(j>2)); //dashed lines for prefit
      myHist->SetBit(kCanDelete);
      myHist->Add( dataHist , -1. ); //subtract the data
      
      //scale each bin based on dataHist error
      for(int k=1;k<=myHist->GetNbinsX();k++) {
        double dataError = 0;//(myHist->GetBinContent(k)<0) ? (dataHist->GetBinContent(k)-0.5*TMath::ChisquareQuantile(TMath::Prob(1,1)/2.,2.*(dataHist->GetBinContent(k)))) : ((0.5*TMath::ChisquareQuantile(1.-TMath::Prob(1,1)/2.,2.*(dataHist->GetBinContent(k)+1))) - dataHist->GetBinContent(k));
        myHist->SetBinContent(k,myHist->GetBinContent(k)/dataError);
        myHist->SetBinError(k,0);
      }
      
      myHist->Draw((j==0)?"":"same");
      myHist->GetYaxis()->SetRangeUser(-2,2);
      gPad->Modified();
      
      
     }
     TLine ll;ll.SetLineStyle(2);ll.DrawLine(dataHist->GetXaxis()->GetXmin(),-1,dataHist->GetXaxis()->GetXmax(),-1);ll.DrawLine(dataHist->GetXaxis()->GetXmin(),1,dataHist->GetXaxis()->GetXmax(),1);
     delete dataHist;
     
     
    } else {
    
      TMultiGraph* allGraphs = new TMultiGraph; allGraphs->SetTitle(Form("%s;%s;%s",chan->GetTitle(),theVar->GetTitle(),( var(chan->GetObservableName(0))->numBins(chan->GetRangeName())==1 || !sOpt.Contains("bins"))?"Integral":"Bin Content - Current Content"));
    
      std::vector<TRooAbsH1*> comps;
      std::vector<TRooAbsH1*> myComps; //created here
      if(!sOpt.Contains("samples")) comps.push_back(chan);
      else {
        //break down by sample ...
        RooFIter fItr = chan->compList().fwdIterator();
        RooAbsArg* arg;
        while( (arg = fItr.next()) ) {
          if(arg->InheritsFrom("TRooAbsH1")) {
            comps.push_back(dynamic_cast<TRooAbsH1*>(arg));
            continue;
          }
          
          //if got here ... we will need to create a temporary TRooH1 for the sample and use that 
          TRooH1D* myComp = new TRooH1D(arg->GetName(),arg->GetTitle(),*var(chan->GetObservableName(0)),chan->GetRangeName());
          double coef = GetSampleCoefficient(arg->GetName());
          for(int i=1;i<=myComp->GetXaxis()->GetNbins();i++) {
            myComp->SetBinContent(i,coef);
          }
          myComp->Scale(*static_cast<RooAbsReal*>(arg));
          
          TString myTitle(myComp->GetTitle());
          // if title matches pattern: L_x_.*_<chan>_.*" then extract the first 'something' as the comp name

          if (myTitle.Contains(TRegexp(TString::Format("L_x_.*_%s_.*",chan->GetName()) ))) {
              myTitle = myTitle(4, myTitle.Index(chan->GetName())-5);
          } else {
              if (myTitle.Contains(TString("_") + chan->GetTitle()))
                  myTitle = myTitle.ReplaceAll(TString("_") + chan->GetTitle(), "");
              myTitle.ReplaceAll("L_x_", "");
              myTitle.ReplaceAll("_overallSyst", "");
              myTitle.ReplaceAll("_x_StatUncert", "");
              myTitle.ReplaceAll("_x_HistSyst", "");
              myTitle.ReplaceAll("_x_Exp", "");
          }
          myComp->SetTitle(myTitle); 
          myComp->SetFillColor(TRooFit::GetColorByName(myTitle,true));
          
          
          myComps.push_back(myComp);
          
          comps.push_back(myComp);
          
        }
      }
      
      for(auto& comp : comps) {
    
        TGraph2D* myGraph = new TGraph2D;
        
        RooRealVar* xVar = var(comp->GetObservableName(0));
        
        //graph needs at least 2 points in x-axis to render correctly delauny triangles
        comp->fillGraph(myGraph,RooArgList(*xVar,*theVar),(xVar->numBins()==1)?2:-1,21);
        myGraph->SetName(comp->GetName());
        myGraph->SetTitle(comp->GetTitle());
        /*myGraph->SetBit(kCanDelete);
        myGraph->Draw(option);*/
        
        //graph2D is now a series of points, split this up into 1D graphs ...
        int nBins = xVar->numBins(comp->GetRangeName());
        
        
        
        if(nBins==1 || myGraph->GetN() == nBins*21) {
          if(!sOpt.Contains("bins")) {
            //integrate the values ... across the bins 
            TGraph* gg = new TGraph;
            
            std::vector<double> pointVals(21,0.);
            for(int k=0;k<nBins;k++) {
              
              for(int j=k*21;j<(k+1)*21;j++) {
                pointVals[j%21] += myGraph->GetZ()[j]*comp->GetBinVolume(k+1) - (sOpt.Contains("samples"))*comp->GetBinContent(k+1); //shift to 0 if doing over samples
                gg->SetPoint(j%21,myGraph->GetY()[j],pointVals[j%21]); 
              }
            }
            allGraphs->Add(gg);
            
          } else {
            if(sOpt.Contains("samples") && !dynamic_cast<RooAbsReal*>(comp)->dependsOn(*theVar)) continue; //don't show non-dependent samples
            for(int k=0;k<nBins;k++) {
              TGraph* gg = new TGraph;
              if(sOpt.Contains("samples")) {
                gg->SetTitle(Form("%s bin %d",comp->GetTitle(),k+1));
              } else {
                gg->SetTitle(Form("Bin %d",k+1));
              }
              for(int j=k*21;j<(k+1)*21;j++) {
                double val = myGraph->GetZ()[j]*comp->GetBinVolume(k+1) - (nBins!=1)*comp->GetBinContent(k+1); //shift so that 0 = nominal if looking at multiple bins
                if(fabs(val)<1e-12) val = 0; //seems to be a slight discrepency between values ... possibly difference in getBinVolume (used in GetBinContent) vs GetBinVolume)
                gg->SetPoint(j%21,myGraph->GetY()[j],val); 
              }
              allGraphs->Add(gg);
            }
          }
        } else {
          Error("DrawDependence","Wrong number of points :-( ");
        }
        
        delete myGraph;
      } //loop over comps
      
      for(int j=0;j<allGraphs->GetListOfGraphs()->GetEntries();j++) {
        TGraph* gg = (TGraph*)allGraphs->GetListOfGraphs()->At(j);
        if(sOpt.Contains("bins")) {
          gg->SetLineColor(j+2);
        } else if(sOpt.Contains("samples")) {
          gg->SetTitle(comps[j]->GetTitle());
          gg->SetLineWidth(2);
          //gg->SetLineColor(j+2);
          if(!dynamic_cast<RooAbsReal*>(comps[j])->dependsOn(*theVar)) gg->SetLineStyle(2); //should we even show non-dependent samples?
          gg->SetLineColor(comps[j]->GetFillColor());
        } else {
          gg->SetLineWidth(2);
        }
      }
      
      
      allGraphs->SetBit(kCanDelete);
      allGraphs->Draw("AL");
      
      for(auto& comp : myComps) delete comp;
      
      
    }
    gPad->Update();
    
  }
  pad->cd(0);

  delete catIter;
}


void TRooWorkspace::Print(Option_t* opt) const {
  TString sOpt(opt);
  sOpt.ToLower();
  
  if(sOpt.Contains("yields")) RooMsgService::instance().getStream(RooFit::INFO).removeTopic(RooFit::NumIntegration); //stop info message every time
  
  if(sOpt.Contains("channels")) {
    //list the channels of this workspace ...
    std::unique_ptr<TIterator> catIter(cat(fChannelCatName)->typeIterator());
    TObject* c;
    while( (c = catIter->Next()) ) {
      TRooAbsHStack* chan = channel(c->GetName());
      RooAbsReal* chanFunc = dynamic_cast<RooAbsReal*>(chan);
      std::cout << c->GetName();
      if(!chan) {
        std::cout << " - INVALID CHANNEL!!!" << std::endl;
        continue;
      }
      if(chanFunc->getAttribute("hidden")) std::cout << " [HIDDEN]";
      if(chanFunc->getAttribute("isValidation")) std::cout << " [BLINDED]";
      if(sOpt.Contains("yields")) {
        double err; 
        double inte = chan->IntegralAndError(err,(fCurrentFitResult)?*fCurrentFitResult:"");
        std::cout << Form(" [ %g +/- %g ]",inte,err);
        
        if(data(fCurrentData)) {
            // FIXME: Generates an InputArguments warning in 6.20 at least??
          double yield = data(fCurrentData)->sumEntries(Form("%s==%s::%s",fChannelCatName.Data(),fChannelCatName.Data(),c->GetName()));
          std::cout << Form(" { %g }", yield);
        }
        
      }
      std::cout << std::endl;
      if(sOpt.Contains("samples")) {
        //if the components are reused, then need to distinguish by printing the coefficient name too ...
        std::set<std::string> compNames;
        bool printCoeffNames(false);
        for(int i=0;i<chan->compList().getSize();i++) { 
          if(compNames.find(chan->compList().at(i)->GetName())!=compNames.end()) { printCoeffNames=true; break; }
          compNames.insert( chan->compList().at(i)->GetName() );
        }
      
        RooFIter fItr = chan->compList().fwdIterator();
        RooAbsArg* arg;
        int i=0;
        while( (arg = fItr.next()) ) {
          std::cout << "   ";
          if(printCoeffNames) std::cout << chan->coeffList().at(i)->GetName() << "*";
          std::cout << arg->GetName();
          if(sOpt.Contains("yields")) {
            double err; 
            double inte = IntegralAndError(err,arg->GetName(),chan->GetName());
            std::cout << Form(" [ %g +/- %g ]",inte,err);
          }
          std::cout << std::endl;
          i++;
        }
      }
    }
  } else if(sOpt.Contains("fits")) {
    //print RooFitResult's saved in the genericObjects list .. displays the fit status codes ... 
    auto genObjs = allGenericObjects();
    for(TObject* obj : genObjs) {
      if(!obj->InheritsFrom(RooFitResult::Class())) continue;
      std::cout << obj->GetName() << " : ";
      RooFitResult* r = dynamic_cast<RooFitResult*>(obj);
      for(uint i=0;i<r->numStatusHistory();i++) {
        if(i!=0) std::cout << " ; ";
        std::cout << r->statusLabelHistory(i) << " = " << r->statusCodeHistory(i);
      }
      std::cout << " ; covQual = " << r->covQual();

      if(fCurrentFit==obj->GetName()) std::cout << " <-- CURRENT FIT";
      std::cout << std::endl;
    }
  } else if(sOpt.Contains("params")) {
    std::map<TString,std::vector<TString>> paramStrings;
    //go through float parameters that are not observables (not in the 'obs' set)
    RooArgSet* _allVars = static_cast<RooArgSet*>(allVars().selectByAttrib("Constant",kFALSE));
    _allVars->remove(*(const_cast<TRooWorkspace*>(this)->set("obs")));
    RooArgSet _allPdfs = allPdfs();
    RooFIter itr = _allVars->fwdIterator();
    RooRealVar* _var = 0;
    while( (_var = dynamic_cast<RooRealVar*>(itr.next())) ) {
      //if(var->isConstant()) continue;if(set("obs")->find(*var)) continue;
      
      TString extraString;
      if(sOpt.Contains("affects")) {
        extraString += " [";
        std::unique_ptr<TIterator> catIter(cat(fChannelCatName)->typeIterator());
        TObject* c;
        std::set<std::string> affectedChans;
        while( (c = catIter->Next()) ) {
          RooAbsReal* chan = function(c->GetName());
          if(!chan) continue;
          if(chan->dependsOn(*_var)) affectedChans.insert( c->GetName() );
        }
        
        if(affectedChans.size() <= uint(cat(fChannelCatName)->numTypes())/2) {
          for(auto& s : affectedChans) {
            if(extraString.Length()!=2) extraString += ",";
            extraString += s;
          }
        } else {
          extraString += "All";
          catIter->Reset();
          while( (c = catIter->Next()) ) {
            if(affectedChans.find(c->GetName())!=affectedChans.end()) continue;
            if(extraString.Length()!=5) extraString += ",";
            else extraString += " except ";
            extraString += c->GetName();
          }
        }
        
        extraString += "]";
      }
      
      //check for constraint term .. which is a pdf featuring just this parameter 
      RooArgSet otherPars(*_allVars); otherPars.remove(*_var);
      RooFIter pitr = _allPdfs.fwdIterator();
      RooAbsPdf* constraintPdf = 0;
      while( RooAbsPdf* _pdf = dynamic_cast<RooAbsPdf*>(pitr.next()) ) {
        if(dynamic_cast<TRooAbsH1*>(_pdf)) continue;
        if(!_pdf->dependsOn(*_var)) continue;
        if(_pdf->dependsOn(otherPars)) continue;
        constraintPdf = _pdf; break;
      }
      if(constraintPdf) {
        if(constraintPdf->InheritsFrom("RooGaussian")) {
          //determine the "mean" (usually a global observable) and the standard deviation ...
          RooGaussian* gPdf = static_cast<RooGaussian*>(constraintPdf);
          double mean=0;
          if(strcmp(gPdf->x.arg().GetName(),_var->GetName())==0) mean = gPdf->mean;
          else mean = gPdf->x;
          double sigma= gPdf->sigma;
          paramStrings["gaussian"].push_back( Form("%s [%g,%g]%s",_var->GetName(),mean,sigma,extraString.Data()) );
        } else if(constraintPdf->InheritsFrom("RooPoisson")) {
          //assume that _var appears multiplicatively inside the mean of the poisson ... so find the server that depends on _var and getVal and divide by _var val to determine tau factor
          //tau factor is square of inverse relatively uncert ... 
          RooFIter sItr = constraintPdf->serverMIterator();
          RooAbsReal* ss;
          while( (ss = (RooAbsReal*)sItr.next()) ) {
            if(ss->dependsOn(*_var)) break;
          }
          if(ss) {
            double tau = ss->getVal() / _var->getVal();
            paramStrings["poisson"].push_back(Form("%s [%g%%]%s",_var->GetName(),100.*1./sqrt(tau),extraString.Data()));
          } else {
            paramStrings["poisson"].push_back(Form("%s%s",_var->GetName(),extraString.Data()));
          }
        } else {
          paramStrings["other"].push_back(Form("%s [%s]%s",_var->GetName(),constraintPdf->ClassName(),extraString.Data()));
        }
      } else {
        if(_var->hasAsymError()) paramStrings["unconstrained"].push_back(Form("%s [%g +/- %g / %g]%s",_var->GetName(),_var->getVal(),_var->getErrorHi(),_var->getErrorLo(),extraString.Data()));
        else if(_var->hasError()) paramStrings["unconstrained"].push_back(Form("%s [%g +/- %g]%s",_var->GetName(),_var->getVal(),_var->getError(),extraString.Data()));
        else paramStrings["unconstrained"].push_back(Form("%s [%g]%s",_var->GetName(),_var->getVal(),extraString.Data()));
      }
      
    }
    delete _allVars;
    std::cout << std::endl;
    std::cout << "Unconstrained parameters (usually floating normalizations and parameters of interest)" << std::endl;
    std::cout << "------------------------" << std::endl;
    for(auto& s : paramStrings["unconstrained"]) {
      std::cout << s;
      if(const_cast<TRooWorkspace*>(this)->set("poi") && const_cast<TRooWorkspace*>(this)->set("poi")->find(s)) std::cout << " [PARAMETER OF INTEREST]";
      std::cout << std::endl;
    }
    if(paramStrings["gaussian"].size()) {
      std::cout << std::endl;
      std::cout << "Gaussian-constrained parameters (usually systematics)  [mean,sigma]" << std::endl;
      std::cout << "------------------------" << std::endl;
      for(auto& s : paramStrings["gaussian"]) {
        std::cout << s << std::endl;
      }
    }
    if(paramStrings["poisson"].size()) {
      std::cout << std::endl;
      std::cout << "Poisson-constrained parameters (usually statistical uncertainties)  [relUncert]" << std::endl;
      std::cout << "------------------------" << std::endl;
      for(auto& s : paramStrings["poisson"]) {
        std::cout << s << std::endl;
      }
    }
    if(paramStrings["other"].size()) {
      std::cout << std::endl;
      std::cout << "Other-constrained parameters" << std::endl;
      std::cout << "------------------------" << std::endl;
      for(auto& s : paramStrings["other"]) {
        std::cout << s << std::endl;
      }
    }
  } else if(sOpt.Contains("data")) {
    auto _allData = allData();
    for(auto d : _allData) {
      std::cout << d->GetName() << " (" << d->numEntries() << " entries)";
      if(d->isWeighted()) std::cout << " (" << d->sumEntries() << " weighted)";
      if(fCurrentData==d->GetName()) std::cout << " <-- CURRENT DATA";
      std::cout << std::endl;
      
    }
  
  } else if(sOpt.Contains("nll")) {
    fNll.Print();
  } else {
    RooWorkspace::Print(opt);
  }
  
}

double TRooWorkspace::sigma_mu(const char* poi, const char* asimovDataset, double asimovValue) {
  RooRealVar* mu = var(poi);
  if(!mu) return -1;
  
  RooArgSet* snap = (RooArgSet*)allVars().snapshot();
  
  
  if(!data(asimovDataset)) {
    //generate asimov dataset (ensure a fit to the obs data is performed first)
    double tmp = mu->getVal();
    mu->setVal(asimovValue);mu->setConstant(true);
    generateAsimov(asimovDataset,Form("Expected Data (%s=%g)",poi,asimovValue),true); //asimov data is automatically added to workspace 
    mu->setVal(tmp);
  }
  
  //check if we have done unconditional fit already ...
  RooFitResult* ucFit = dynamic_cast<RooFitResult*>( obj(Form("fitTo_%s",asimovDataset)) );
  if(!ucFit || ucFit->constPars().find(poi)) {
    //need to ensure mu is floating ...
    double tmp = mu->getVal();
    TString oldData = fCurrentData; 
    TString oldFit = fCurrentFit;
    bool oldFitPrefit = fCurrentFitIsPrefit;
    mu->setConstant(false);
    auto fit = fitTo(asimovDataset,"qnr");
    mu->setVal(tmp);
    fCurrentData = oldData;
    if(oldFit!="") loadFit(oldFit,oldFitPrefit);
    else {
      //load nominal values if no previous fit was loaded
      loadFit(0);
    }
    allVars() = *snap;
    ucFit = fit; // - FIXME: when got validation channels the name is modified because of change to dataset name etc//dynamic_cast<RooFitResult*>( obj(Form("fitTo_%s",asimovDataset)) );
  }
  
  
  //now need a conditional fit ...
  mu->setConstant(true);
  RooFitResult* cFit = fitTo(data(asimovDataset),getSnapshot(Form("%s_globalObservables",asimovDataset)),"qn"); //avoids saving result into workspace
  
  allVars() = *snap;
  delete snap;
  
  double pll_asimov = 2.0*(cFit->minNll() - ucFit->minNll());
  
  delete cFit;
  
  if(pll_asimov < 1e-15) pll_asimov = 1e-15; //protect against 0 and negative values
  
  return  (fabs(mu->getVal() - asimovValue) / sqrt(pll_asimov) );
  
}

//returns null and alt p value
//This involves running up to 5 fits:
//  Evaluating sigma_mu requires:
//    1. Conditional fit of asimov data (to evaluate asimov pll)
//    2. Unconditional fit of asimov data is also required if not already saved in workspace
//  If need to generate asimov data then:
//    3. Conditional fit to current data required
//  Evaluating pll is up to two fits:
//    4. Unconditional fit to provided data
//    5. Conditional fit to provided data if required by compatibility function 
std::pair<double,double> TRooWorkspace::asymptoticPValue(const char* poi, RooAbsData* data, const RooArgSet* _gobs,bool (*_compatibilityFunction)(double mu, double mu_hat), double alt_val, double** _sigma_mu, RooFitResult** _unconditionalFit) {

  if(fabs(var(poi)->getVal() - alt_val) < 1e-9) {
    //shift alt_val so that it's different to testing value ...
    if(var(poi)->getVal() - 1. >= var(poi)->getMin()) alt_val -= 1.;
    else alt_val += 0.1;
  }

  double sigma = 0;

  if(_sigma_mu && *_sigma_mu) sigma = **_sigma_mu;
  if(sigma<=0) {
    sigma = sigma_mu(poi,TString::Format("asimovData%g",alt_val),alt_val);
    if(_sigma_mu) *_sigma_mu = new double(sigma); //updates _sigma_mu
  }
  
  double _pll = pll(*var(poi),data,_gobs,_compatibilityFunction,0,_unconditionalFit);
  
  if(std::isnan(_pll)) {
    return std::make_pair( std::numeric_limits<double>::quiet_NaN() , std::numeric_limits<double>::quiet_NaN() );
  }
  
  //std::cout << _pll << " " << sigma << std::endl;
  
  
  
  return std::make_pair( TRooFit::Asymptotics::nullPValue(_pll, var(poi), sigma, _compatibilityFunction) , 
                         TRooFit::Asymptotics::altPValue(_pll, var(poi), alt_val, sigma, _compatibilityFunction) );

}

#include "Math/BrentRootFinder.h"
#include "Math/WrappedFunction.h"
#include "Math/ProbFunc.h"

struct TailIntegralFunction { 
  TailIntegralFunction( RooRealVar* _poi, double _alt_val, double _sigma_mu, bool (*_compatibilityFunction)(double mu, double mu_hat), double _target ) : 
   poi(_poi), alt_val(_alt_val), sigma_mu(_sigma_mu), target(_target), compatibilityFunction(_compatibilityFunction)
   {}
  double operator() (double x) const {
    return TRooFit::Asymptotics::altPValue(x,poi,alt_val,sigma_mu,compatibilityFunction) - target;
  }
  RooRealVar* poi;
  double alt_val, sigma_mu, target;
  bool (*compatibilityFunction)(double mu, double mu_hat);
  
};

std::pair<double,double> TRooWorkspace::asymptoticExpectedPValue(const char* poi, bool (*_compatibilityFunction)(double mu, double mu_hat), double nSigma, double alt_val, double** _sigma_mu) {
  
  //need to determine the expected pll value corresponding to nSigma
  
  double sigma = 0;
  
  

  if(_sigma_mu && *_sigma_mu) sigma = **_sigma_mu;
  if(sigma<=0) {
    double orig_alt_val = alt_val;
    if(fabs(var(poi)->getVal() - alt_val) < 1e-9) {
      //shift alt_val so that it's different to testing value ...
      if(var(poi)->getVal() - 1. >= var(poi)->getMin()) alt_val -= 1.;
      else alt_val += 1.;
    }
  
    sigma = sigma_mu(poi,TString::Format("asimovData%d",int(alt_val)),alt_val);
    if(_sigma_mu) *_sigma_mu = new double(sigma); //updates _sigma_mu
    
    alt_val = orig_alt_val;
    
  }
  
  //find the solution (wrt x) of: TRooFit::Asymptotics::altPValue(x, var(poi), alt_val, _sigma_mu, _compatibilityFunction) - targetPValue = 0 
  float targetTailIntegral = ROOT::Math::normal_cdf(nSigma);
  
  TailIntegralFunction f( var(poi), alt_val, sigma, _compatibilityFunction  , targetTailIntegral );
  ROOT::Math::BrentRootFinder brf;
  ROOT::Math::WrappedFunction<TailIntegralFunction> wf(f);

  auto tmpLvl = gErrorIgnoreLevel;
  gErrorIgnoreLevel = kFatal;
  double _pll(500.);
  double currVal(1.);
  int tryCount(0);
  while( fabs( TRooFit::Asymptotics::altPValue(_pll, var(poi), alt_val, sigma, _compatibilityFunction) - targetTailIntegral ) > 1e-4 ) {
    currVal = TRooFit::Asymptotics::altPValue(_pll, var(poi), alt_val, sigma, _compatibilityFunction);
    if(currVal - targetTailIntegral > 1e-4) _pll = 2.*(_pll+1.);
    else if(currVal - targetTailIntegral < -1e-4) _pll /= 2.;
    //std::cout << "pll = " << _pll << " currVal = " << currVal << std::endl;
    brf.SetFunction( wf, 0, _pll);
    if(brf.Solve()) {
        double _prev_pll = _pll;
        _pll =  brf.Root();
        if (_prev_pll < 1 && fabs(_pll) < 1e-9) break; // solution appears to be right up against
                                                     // the y-axis
    }
    //std::cout << " -- " << brf.Root() << " " << TRooFit::Asymptotics::altPValue(_pll, var(poi), alt_val, sigma, _compatibilityFunction) << std::endl;
    tryCount++;
    if(tryCount>20) {
        gErrorIgnoreLevel = tmpLvl;
        Warning("asymptoticExpectedPValue","Reached limit nSigma=%g pll=%g",nSigma,_pll);
      break;
    }
  
  }
  gErrorIgnoreLevel = tmpLvl;
  _pll *= 0.99; //subtract a little to capture delta function effects

  if(fabs(_pll) < 1e-9) return std::make_pair(1,1);

  return std::make_pair( TRooFit::Asymptotics::nullPValue(_pll, var(poi), sigma, _compatibilityFunction) , 
                         TRooFit::Asymptotics::altPValue(_pll, var(poi), alt_val, sigma, _compatibilityFunction) );
  
}

//report variations above a given relative uncertainty for any TRooAbsH1 component
void TRooWorkspace::FindVariations(double relThreshold, Option_t* opt) {
  TString sOpt(opt);
  sOpt.ToLower();
  
  RooAbsArg* arg;

  RooArgSet _allFuncs = allPdfs();
  
  RooArgSet _allVars = allVars();
  RooFIter vitr = _allVars.fwdIterator();
  while( (arg = vitr.next()) ) {
    RooRealVar* v = dynamic_cast<RooRealVar*>(arg);
    if(v->isConstant() || v->getError()<1e-9) continue;
    std::cout << v->GetName() << ":";
    RooFIter itr = _allFuncs.fwdIterator();
    while( (arg = itr.next()) ) {
      if(!arg->dependsOn(*v)) continue;
      TRooAbsH1* f = dynamic_cast<TRooAbsH1*>(arg);
      if(!f) continue;
      
      if(dynamic_cast<TRooAbsHStack*>(f) && !sOpt.Contains("channels")) continue;
      else if(!dynamic_cast<TRooAbsHStack*>(f) && !sOpt.Contains("samples")) continue;
      
      //loop over bins of function, check for variation in bin content above threshold
      bool printedName=false;
      for(int i=1;i<=f->GetNbinsX();i++) {
        double nomVal = f->GetBinContent(i);
        double tmpVal = v->getVal();
        v->setVal(tmpVal + v->getErrorHi());
        double upVal = f->GetBinContent(i);
        v->setVal(tmpVal + v->getErrorLo());
        double downVal = f->GetBinContent(i);
        v->setVal(tmpVal);
        if(fabs(nomVal)<1e-9) {nomVal+=1e-9; upVal+=1e-9;downVal+=1e-9;}
        
        if(!sOpt.Contains("ss"))  {
          if(fabs((upVal-nomVal)/nomVal) > relThreshold) {
            if(!printedName) { std::cout << std::endl << " " << f->GetName() << " :"; printedName=true; }
            std::cout << " " << i << "UP(" << (upVal-nomVal)/nomVal << ")";
          }
          if(fabs((downVal-nomVal)/nomVal) > relThreshold) {
            if(!printedName) { std::cout << std::endl << f->GetName() << " :"; printedName=true; }
            std::cout << " " << i << "DOWN(" << (downVal-nomVal)/nomVal << ")";
          }
        } else {
          if( ((upVal < nomVal && downVal < nomVal) || (upVal > nomVal && downVal > nomVal)) && ( fabs((upVal-nomVal)/nomVal) > relThreshold || fabs((downVal-nomVal)/nomVal) > relThreshold ) ) {
            if(!printedName) { std::cout << std::endl << " " << f->GetName() << " :"; printedName=true; }
            std::cout << " " << i << "SS(" << (upVal-nomVal)/nomVal << "," << (downVal-nomVal)/nomVal << ")";
          }
        }
      }
    }
    std::cout << std::endl;
    
  }
  
  

}


void TRooWorkspace::setDefaultStyle() {
  
  gStyle->SetOptStat(0);

  Int_t icol=0;
  gStyle->SetFrameBorderMode(icol);
  gStyle->SetFrameFillColor(icol);
  gStyle->SetCanvasBorderMode(icol);
  gStyle->SetCanvasColor(icol);
  gStyle->SetPadBorderMode(icol);
  gStyle->SetPadColor(icol);
  gStyle->SetStatColor(icol);
  gStyle->SetPaperSize(20,26);
  gStyle->SetPadTopMargin(0.05);
  gStyle->SetPadRightMargin(0.15);
  gStyle->SetPadBottomMargin(0.15);
  gStyle->SetPadLeftMargin(0.15);
  gStyle->SetTitleXOffset(1.0);
  gStyle->SetTitleYOffset(1.0);
  Int_t font=42;//43; changed to relative because on multiplots with panels in vertical direction, the offset wasn't right for absolute font sizes
  Double_t tsize=0.045;//18;
   Double_t lsize = 0.04;//15;
  gStyle->SetTextFont(font);
  gStyle->SetTextSize(tsize);
  gStyle->SetLabelFont(font,"x");
  gStyle->SetTitleFont(font,"x");
  gStyle->SetLabelFont(font,"y");
  gStyle->SetTitleFont(font,"y");
  gStyle->SetLabelFont(font,"z");
  gStyle->SetTitleFont(font,"z");
  gStyle->SetLabelSize(lsize,"x");
  gStyle->SetTitleSize(tsize,"x");
  gStyle->SetLabelSize(lsize,"y");
  gStyle->SetTitleSize(tsize,"y");
  gStyle->SetLabelSize(lsize,"z");
  gStyle->SetTitleSize(tsize,"z");
  gStyle->SetMarkerStyle(20);
  gStyle->SetMarkerSize(0.5);
  gStyle->SetHistLineWidth(1);
  gStyle->SetLineStyleString(2,"[12 12]");
  gStyle->SetEndErrorSize(0.);
  gStyle->SetOptTitle(0);
  gStyle->SetOptStat(1111111);
  gStyle->SetOptFit(0);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  gStyle->SetOptStat(0);
  

  gStyle->SetLegendFont(font);

   const Int_t NRGBs = 5;
   const Int_t NCont = 255;

   Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
   Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
   Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
   Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
   TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
   gStyle->SetNumberContours(NCont);
}

void TRooWorkspace::Scale(const char *sampleName, const char *channelNames, const char *par) {
    if(var(par)) Scale(sampleName,channelNames,*var(par));
    else if(function(par)) Scale(sampleName,channelNames,*function(par));
    else {
        Error("Scale","Could not find scale factor %s",par);
    }
}

bool TRooWorkspace::addSaturatingSamples(const char* name, const char* title, const char* channels,
        bool allowNegative, int firstbin, int lastbin) {

    bool out = addSamples(name,title,channels,allowNegative);
    if (!out) return out;

    // now go through the added samples
    // set bin content to 1 and create a shape factor for it
    std::unique_ptr<TIterator> catIter(cat(fChannelCatName)->typeIterator());
    TObject* c;
    while( (c = catIter->Next()) ) {
        auto _sample = sample(name,c->GetName());
        if(!_sample) continue;
        int maxbin = (lastbin==0) ? _sample->GetNbinsX() : lastbin;
        for(int i=std::max(firstbin,1); i<=std::min(maxbin,_sample->GetNbinsX());i++) {
            _sample->SetBinContent(i,1);
            TString parName = Form("%s_bin%d",_sample->GetName(),i);
            //TODO: Make range of parameter dependent on context
            auto par = addParameter(parName,parName,0,(allowNegative)?-50:0,50);
            if (!par) {
                Error("addSaturatingSamples","Failed to create %s", parName.Data());
            } else {
                _sample->addShapeFactor(i, *par);
            }
        }
    }

    return out;


}

bool TRooWorkspace::Add(TObject& obj) {

    if (dynamic_cast<TH1 *>(&obj)) {
        return _add(*dynamic_cast<TH1 *>(&obj));
    } else if (dynamic_cast<TAxis *>(&obj)) {
        return _add(*dynamic_cast<TAxis *>(&obj));
    }
    return false;
}

bool TRooWorkspace::_add(TAxis& ax) {
    return addVariable(ax);
}

bool TRooWorkspace::_add(TH1& h1) {

    // determine sample_name, channel_name, and any variation etc
    TString s_sample_name, s_channel_name, s_variation_name;
    double variation_val = 1;
    s_sample_name = h1.GetName();
    if (s_sample_name.Contains(';')) {
        s_channel_name = s_sample_name(s_sample_name.Index(';')+1,s_sample_name.Length());
        s_sample_name = s_sample_name(0,s_sample_name.Index(';'));
    }
    if (s_channel_name.Contains(';')) {
        s_variation_name = s_channel_name(s_channel_name.Index(';')+1,s_channel_name.Length());
        s_channel_name = s_channel_name(0,s_channel_name.Index(';'));
    }
    if (s_variation_name.Contains('=')) {
        variation_val = TString(s_variation_name(s_variation_name.Index('=')+1,s_variation_name.Length())).Atof();
        s_variation_name = s_variation_name(0,s_variation_name.Index('='));
    }

    const char* sample_name = s_sample_name.Data();
    const char* channel_name = s_channel_name.Data();
    const char* variation_name = s_variation_name.Data();

    if(variation_name && strlen(variation_name)>0 && !var(variation_name)) {
        Info("Add","%s is not defined. Creating a gaussian-constrained parameter",variation_name);
        addParameter(variation_name,variation_name,0,-5,5,"normal");
    }

    TAxis* ax = h1.GetXaxis();
    TString axname = ax->GetName();
    if (axname == "xaxis") {
        ax->SetName(axname+"_"+channel_name);
    }

    // user channel_name as binning name, unless none given (i.e. is a factor) then use factor name
    if (!channel_name || strlen(channel_name) == 0) {
        ax->SetName(TString(ax->GetName()) + ";" + sample_name);
    } else {
        ax->SetName(TString(ax->GetName()) + ";" + channel_name);
    }
    auto v = addVariable(*ax);
    if (!v) {
        Error("Add", "Failed to create observable %s", ax->GetName());
        return false;
    }

    if (!channel_name || strlen(channel_name) == 0) {
        // the empty channelName is how to refer to Factors instead of Samples
        // Factors don't belong to any specific channel
        if (!factor(sample_name)) {
            Info("Add", "Creating factor %s with observable %s from histogram",sample_name,ax->GetName());

            if (!addFactor(sample_name, sample_name, ax, 0 /* initial val*/)) {
                Error("Add", "Failed to create factor %s", sample_name);
                return false;
            }
            factor(sample_name)->setUnit(h1.GetYaxis()->GetTitle()); // sets y-axis title
        }
        return factor(sample_name)->Add(&h1, 1., strlen(variation_name) ? var(variation_name) : 0,
                                       variation_val);
    }


    // check the sample exists .. if it doesn't, we will use the histogram to create it ...
    if (!channel(channel_name)) {
        if (!addChannel(channel_name,channel_name,ax,h1.TestBit(TRooFit::kCreatePdfChannel))) {
            Error("Add","Failed to create channel %s", channel_name);
            return false;
        }
    }

    if(s_sample_name.Length()==0) {
        //interpret as adding data ...
        auto current_data = data(fCurrentData);
        for(int i=1;i<=h1.GetNbinsX();i++) {
            Fill(channel_name,h1.GetXaxis()->GetBinCenter(i),h1.GetBinContent(i));
        }
        if (!current_data) {
            // should have been created ... transfer title to dataset
            current_data = data(fCurrentData);
            if (current_data) {
                current_data->SetTitle(h1.GetTitle());
            }
        }
        return true;
    }

    if(!sample(sample_name,channel_name)) {
        Info("Add","Creating sample %s in channel %s",sample_name,channel_name);
        addSamples(sample_name,h1.GetTitle(),
                channel_name,
                h1.TestBit(TRooFit::kAllowNegative),
                !h1.TestBit(TRooFit::kKeepStatFactors));
        if (!sample(sample_name,channel_name)) {
            Error("Add","Failed to create sample %s",sample_name);
            return false;
        }
        // constructor of TRooH1 will have added a binning to variable, set the title appropriately
        v->getBinning(s_sample_name+"_"+channel_name).SetTitle(ax->GetTitle());
    }

    return sample(sample_name,channel_name)->Add(&h1, 1.,
            strlen(variation_name) ? var(variation_name) : 0,
            variation_val);

}