//
// Created by Will Buttinger on 2019-10-13.
//

#include "RooFit.h"

#include "Rtypes.h"
#include "Riostream.h"
#include "TEnv.h"

#include "TRooVersion.h"


using namespace std;

Int_t doTRooBanner();

static Int_t dummyTRoo = doTRooBanner() ;

Int_t doTRooBanner()

{
#ifndef __TROOFIT_NOBANNER
             cout << "\033[1mTRooFit      -- Developed by Will Buttinger\033[0m " << endl
             << "                ROOT-inspired RooFit Expansion : http://gitlab.cern"
                ".ch/will/TRooFit" << endl << "                version: " << GIT_COMMIT_HASH <<
                endl;
#endif
    (void) dummyTRoo;
    return 0 ;
}
